package com.fdariasm.nomatags.visitors;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Square;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;
import com.fdariasm.nomatags.strategies.android.ResourceLocator;
import com.fdariasm.nomatags.utils.TwoTuple;

public class DrawVisitor implements ShapeVisitor{
	
	
	private ResourceLocator mLocator;
	
	public DrawVisitor(ResourceLocator locator, Presentation presentation) {
		mLocator = locator;
	}
	
	private void restorePaint(BaseShape shape){
		Paint paint = mLocator.getPaint();
		paint.setStrokeWidth(shape.getStrokeWidth());
		paint.setColor(shape.getColor());
	}
	
	@Override
	public void visitLine(Line line) {
		restorePaint(line);
		Point start = line.getStart();
		Point end = line.getEnd();
		mLocator.getCanvas().drawLine(start.getX(), start.getY(),
				end.getX(), end.getY(), mLocator.getPaint());
	}


	@Override
	public void visitCurve(Curve curve) {
		restorePaint(curve);
		DrawShapeStrategy strategy = curve.getStrategy();
		strategy.draw();
	}


	@Override
	public void visitSquare(Square square) {
		restorePaint(square);
		TwoTuple<Point> pointsOrder =
				getPointsOrder(square.getStart(), square.getEnd());
		Point start = pointsOrder.first;
		Point end = pointsOrder.second;
		mLocator.getCanvas().drawRect(start.getX(), start.getY(),
				end.getX(), end.getY(), mLocator.getPaint());
	}


	@Override
	public void visitImage(Image image) {
		Canvas canvas = mLocator.getCanvas();
		Point position = image.getPosition();
		canvas.drawBitmap(image.getImage(), 
				position.getX(), position.getY(), null);
	}


	@Override
	public void visitEllipse(Ellipse square) {
		restorePaint(square);
		Point start = square.getStart();
		Point end = square.getEnd();
		mLocator.getCanvas().drawOval(getRect(start, end), mLocator.getPaint());
	}
	
	public static TwoTuple<Point> getPointsOrder(Point a, Point b){
		float startX;
		float startY;
		float endX;
		float endY;
		if(a.getX() < b.getX()){
			startX = a.getX();
			endX = b.getX();					
		}else{
			startX = b.getX();
			endX = a.getX();
		}
		if(a.getY() < b.getY()){
			startY = a.getY();
			endY = b.getY();					
		}else{
			startY = b.getY();
			endY = a.getY();
		}
		return new TwoTuple<Point>(new Point(startX, startY), 
				new Point(endX, endY));
	}

	private static RectF getRect(Point startPoint, Point endPoint){
		float startX;
		float startY;
		float endX;
		float endY;
		if(startPoint.getX() < endPoint.getX()){
			startX = startPoint.getX();
			endX = endPoint.getX();
		}else{
			startX = endPoint.getX();
			endX = startPoint.getX();
		}
		if(startPoint.getY() < endPoint.getY()){
			startY = startPoint.getY();
			endY = endPoint.getY();
		}else{
			startY = endPoint.getY();
			endY = startPoint.getY();
		}

		RectF rect = new RectF();
		rect.top = startY;
		rect.left = startX;
		rect.right = endX;
		rect.bottom = endY;
		return rect;
	}
}

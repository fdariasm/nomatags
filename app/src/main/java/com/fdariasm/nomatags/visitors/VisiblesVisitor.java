package com.fdariasm.nomatags.visitors;

import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Square;

import java.util.LinkedList;

public class VisiblesVisitor implements ShapeVisitor{

	private LinkedList<BaseShape> visibles;
	
	public VisiblesVisitor() {
		visibles = new LinkedList<BaseShape>();
	}
	
	@Override
	public void visitLine(Line line) {
		visibles.add(line);
	}

	@Override
	public void visitCurve(Curve curve) {
		visibles.add(curve);
	}

	@Override
	public void visitSquare(Square square) {
		visibles.add(square);
	}

	@Override
	public void visitImage(Image square) {
		visibles.add(square);
	}

	@Override
	public void visitEllipse(Ellipse square) {
		visibles.add(square);
	}
	
	public void setVisible(LinkedList<BaseShape> visible) {
		this.visibles = visible;
	}

	public LinkedList<BaseShape> getVisible() {
		return visibles;
	}


}

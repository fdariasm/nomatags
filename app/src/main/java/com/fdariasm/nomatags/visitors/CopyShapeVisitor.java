package com.fdariasm.nomatags.visitors;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.ShapeFactory;
import com.fdariasm.nomatags.models.components.Square;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;

import java.util.LinkedList;

public class CopyShapeVisitor implements ShapeVisitor{
	
	private LinkedList<BaseShape> copies = new LinkedList<BaseShape>();
	private ShapeFactory shapeFactory;
	
	public CopyShapeVisitor(AppState appState) {
		shapeFactory = new ShapeFactory(appState);
	}

	@Override
	public void visitLine(Line line) {
		Line newLine = shapeFactory.createLine();
		copyBase(line, newLine);
		newLine.setStart(new Point(line.getStart()));
		newLine.setEnd(new Point(line.getEnd()));
		
		copies.add(newLine);
	}
	
	public LinkedList<BaseShape> getCopy(){
		return copies;
	}


	@Override
	public void visitCurve(Curve curve) {
		DrawShapeStrategy nStrategy = curve.getStrategy().copy();
		Curve nCurve = shapeFactory.createCurve(nStrategy);
		copyBase(curve, nCurve);
		
		for(Point p : curve.getPoints()){
			nCurve.addPoint(new Point(p));
		}
		copies.add(nCurve);
	}

	@Override
	public void visitSquare(Square square) {
		Square nSquare = shapeFactory.createSquare();
		copyBase(square, nSquare);
		nSquare.setStart(new Point(square.getStart()));
		nSquare.setEnd(new Point(square.getEnd()));
		copies.add(nSquare);
	}


	@Override
	public void visitImage(Image square) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitEllipse(Ellipse square) {
		Ellipse newEllipse= shapeFactory.createEllipse();
		copyBase(square, newEllipse);
		newEllipse.setStart(new Point(square.getStart()));
		newEllipse.setEnd(new Point(square.getEnd()));

		copies.add(newEllipse);
	}
	
	private void copyBase(BaseShape src, BaseShape dst){
		dst.setColor(src.getColor());
		dst.setStrokeWidth(src.getStrokeWidth());
		dst.addListeners(src.getListeners());
	}


}

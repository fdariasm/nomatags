package com.fdariasm.nomatags.visitors;

import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Square;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;

public class MoveShapeVisitor implements ShapeVisitor{

	private float dx;
	private float dy;
	
	public MoveShapeVisitor(float dx, float dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	@Override
	public void visitLine(Line line) {
		line.setStart(line.getStart().movePoint(dx, dy));
		line.setEnd(line.getEnd().movePoint(dx, dy));
	}


	@Override
	public void visitCurve(final Curve curve) {

		DrawShapeStrategy strategy = curve.getStrategy();
		strategy.offset(dx, dy);
		offset(curve);
		//use thread?
//		new Thread(new Runnable(){
//
//			@Override
//			public void run() {
//				offset(curve);
//			}
//			
//		}).start();
	}
	
	private void offset(Curve curve){
		for(Point p : curve.getPoints()){
			p.movePoint(dx, dy);
		}
		curve.setPoints(curve.getPoints());
	}

	@Override
	public void visitSquare(Square square) {
		square.setStart(square.getStart().movePoint(dx, dy));
		square.setEnd(square.getEnd().movePoint(dx, dy));
	}


	@Override
	public void visitImage(Image image) {
		Point position = image.getPosition();
		image.setPosition(position.movePoint(dx, dy));
	}

	@Override
	public void visitEllipse(Ellipse square) {
		square.setStart(square.getStart().movePoint(dx, dy));
		square.setEnd(square.getEnd().movePoint(dx, dy));
	}

}

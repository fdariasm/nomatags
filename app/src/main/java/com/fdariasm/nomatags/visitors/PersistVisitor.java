package com.fdariasm.nomatags.visitors;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Environment;

import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Square;
import com.fdariasm.nomatags.models.process.SaveDocument;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PersistVisitor implements ShapeVisitor{

	private final DocumentShapes document;
	private final SQLiteDatabase wdb;

    public static final String EXTERNAL_FOLDER = "Nomatags";
    public static final String IMG_FOLDER = "imported_images";

	public PersistVisitor(SQLiteDatabase wdb, DocumentShapes document){
		this.wdb = wdb;
		this.document = document;
	}

	@Override
	public void visitLine(Line line) {
        long insertedShape = insertBaseShape(
                SaveDocument.Types.LINE, line);

        insertLSE(line.getStart(), line.getEnd(), insertedShape);
	}

    private long insertBaseShape(SaveDocument.Types type, BaseShape bs){
        ContentValues values = new ContentValues();

        values.put(SaveDocument.BaseShapeTable.COLUMN_NAME_TYPE, type.id);
        values.put(SaveDocument.BaseShapeTable.COLUMN_NAME_DOCUMENT, document.getId());
        values.put(SaveDocument.BaseShapeTable.COLUMN_NAME_COLOR, bs.getColor());
        values.put(SaveDocument.BaseShapeTable.COLUMN_NAME_STROKE, bs.getStrokeWidth());
        values.put(SaveDocument.BaseShapeTable.COLUMN_NAME_DOCUMENT_SEQ, bs.getSeqNumber());

        return wdb.insert(SaveDocument.BaseShapeTable.NAME, null, values);
    }

    private void insertLSE(Point start, Point end, long insertedShape){
        ContentValues values = new ContentValues();
        values.put(SaveDocument.LSETable.COLUMN_NAME_START_X, start.getX());
        values.put(SaveDocument.LSETable.COLUMN_NAME_START_Y, start.getY());
        values.put(SaveDocument.LSETable.COLUMN_NAME_END_X, end.getX());
        values.put(SaveDocument.LSETable.COLUMN_NAME_END_Y, end.getY());
        values.put(SaveDocument.LSETable.COLUMN_NAME_SHAPE, insertedShape);

        wdb.insert(SaveDocument.LSETable.NAME, null, values);
    }

	@Override
	public void visitCurve(Curve curve) {

        long insertedShape = insertBaseShape(
                SaveDocument.Types.CURVE, curve);

        for(Point p : curve.getPoints()){
            ContentValues values = new ContentValues();
            values.put(SaveDocument.CurveTable.COLUMN_NAME_POS_X, p.getX());
            values.put(SaveDocument.CurveTable.COLUMN_NAME_POS_Y, p.getY());

            values.put(SaveDocument.CurveTable.COLUMN_NAME_SHAPE, insertedShape);

            wdb.insert(SaveDocument.CurveTable.NAME, null, values);
        }
	}

	@Override
	public void visitSquare(Square square) {
		long insertedShape = insertBaseShape(
				SaveDocument.Types.SQUARE, square);

		insertLSE(square.getStart(), square.getEnd(), insertedShape);
	}

	@Override
	public void visitImage(Image image) {
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/" + EXTERNAL_FOLDER + "/" + IMG_FOLDER;
        File dir = new File(file_path);
        if(!dir.exists())
            dir.mkdirs();
        File file = new File(dir, "iimg" + document.getId() + "_" + image.getSeqNumber() + ".png");
        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(file);

            image.getImage().compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        long insertedShape = insertBaseShape(SaveDocument.Types.IMAGE, image);

        Point p = image.getPosition();

        ContentValues values = new ContentValues();
        values.put(SaveDocument.ImageTable.COLUMN_NAME_POS_X, p.getX());
        values.put(SaveDocument.ImageTable.COLUMN_NAME_POS_Y, p.getY());
        values.put(SaveDocument.ImageTable.COLUMN_NAME_IMAGE_URI, file.getAbsolutePath());

        values.put(SaveDocument.ImageTable.COLUMN_NAME_SHAPE, insertedShape);

        wdb.insert(SaveDocument.ImageTable.NAME, null, values);
    }

	@Override
	public void visitEllipse(Ellipse ellipse) {
		long insertedShape = insertBaseShape(
				SaveDocument.Types.ELLIPSE, ellipse);

		insertLSE(ellipse.getStart(), ellipse.getEnd(), insertedShape);
	}

}

package com.fdariasm.nomatags.visitors;

import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Square;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;

import java.util.LinkedList;

public class ScaleShapeVisitor implements ShapeVisitor {
	
	private float scaleFactor;
	private Point referencePoint;
	
	public ScaleShapeVisitor(float scaleFactor, Point referencePoint) {
		this.scaleFactor = scaleFactor;
		this.referencePoint = referencePoint;
	}

	@Override
	public void visitLine(Line line) {
		
		Point start = line.getStart();
		Point end = line.getEnd();
		start.scale(scaleFactor, referencePoint);
		end.scale(scaleFactor, referencePoint);
		line.notifyListeners();
		
	}

	@Override
	public void visitCurve(Curve curve) {
		//FIXME move points in thread or executor
		DrawShapeStrategy strategy = curve.getStrategy();
		strategy.scale(scaleFactor, referencePoint);
		LinkedList<Point> points = curve.getPoints();
		for(Point p : points){
			p.scale(scaleFactor, referencePoint);
		}
		curve.notifyListeners();
	}

	@Override
	public void visitSquare(Square square) {
		Point start = square.getStart();
		Point end = square.getEnd();
		start.scale(scaleFactor, referencePoint);
		end.scale(scaleFactor, referencePoint);
		square.notifyListeners();
	}

	@Override
	public void visitImage(Image square) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitEllipse(Ellipse ellipse) {
		Point start = ellipse.getStart();
		Point end = ellipse.getEnd();
		start.scale(scaleFactor, referencePoint);
		end.scale(scaleFactor, referencePoint);
		ellipse.notifyListeners();
	}

}

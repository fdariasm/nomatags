package com.fdariasm.nomatags.visitors;

import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Square;


public interface ShapeVisitor {
	
	void visitLine(Line line);

	void visitCurve(Curve curve);

	void visitSquare(Square square);

	void visitImage(Image square);
	
	void visitEllipse(Ellipse square);
}

package com.fdariasm.nomatags.visitors;

import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Square;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;

import java.util.LinkedList;

public class RotateShapeVisitor implements ShapeVisitor{
	
	private float radians;
	private float degrees;
	private Point referencePoint;
	private double sinA;
	private double cosA;
	
	

	public RotateShapeVisitor(float radians, Point referencePoint) {
		this.radians = radians;
		this.referencePoint = referencePoint;
		
		degrees = (float) Math.toDegrees(radians);
		
		cosA = Math.cos(this.radians);
		sinA = Math.sin(this.radians);
	}

	@Override
	public void visitLine(Line line) {
		Point start = line.getStart();
		Point end = line.getEnd();
		start.rotate(referencePoint, cosA, sinA);
		end.rotate(referencePoint, cosA, sinA);
		
		line.notifyListeners();
	}


	@Override
	public void visitCurve(Curve curve) {
		DrawShapeStrategy strategy = curve.getStrategy();
		strategy.rotate(degrees, referencePoint);
		LinkedList<Point> points = curve.getPoints();
		for(Point p : points){
			p.rotate(referencePoint, cosA, sinA);
		}
		curve.setPoints(points);
	}


	@Override
	public void visitSquare(Square square) {
		Point start = square.getStart();
		Point end = square.getEnd();
		start.rotate(referencePoint, cosA, sinA);
		end.rotate(referencePoint, cosA, sinA);

		square.notifyListeners();
	}

	@Override
	public void visitImage(Image square) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visitEllipse(Ellipse ellipse) {
		Point start = ellipse.getStart();
		Point end = ellipse.getEnd();
		start.rotate(referencePoint, cosA, sinA);
		end.rotate(referencePoint, cosA, sinA);

		ellipse.notifyListeners();
	}

}

package com.fdariasm.nomatags.visitors;

import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.models.components.Square;

import java.util.LinkedList;
import java.util.List;

public class SelectVisitor implements ShapeVisitor{

	
	private List<BaseShape> selected;
	
	private AreaSelectionVisitor areaSelection;
	
	public SelectVisitor() {
		selected = new LinkedList<BaseShape>();
		areaSelection = new AreaSelectionVisitor();
	}
	
	public List<BaseShape> getShapesSelected() {
		return selected;
	}
	
	public Rectangle getAreaSelected(){
		return areaSelection.getSelectedArea();
	}
	
	@Override
	public void visitLine(Line line) {
		selected.add(line);
		areaSelection.visitLine(line);
	}

	@Override
	public void visitCurve(Curve curve) {
		selected.add(curve);
		areaSelection.visitCurve(curve);
	}

	@Override
	public void visitSquare(Square square) {
		selected.add(square);
		areaSelection.visitSquare(square);
	}

	@Override
	public void visitImage(Image image) {
		selected.add(image);
		areaSelection.visitImage(image);
	}

	@Override
	public void visitEllipse(Ellipse ellipse) {
		selected.add(ellipse);
		areaSelection.visitEllipse(ellipse);
	}

}

package com.fdariasm.nomatags.visitors;

import android.util.Log;

import com.fdariasm.nomatags.controllers.actions.SelectionHandler;
import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.models.components.Square;

import java.util.LinkedList;

public class AreaSelectionVisitor implements ShapeVisitor{

	private static final String TAG = AreaSelectionVisitor.class.getSimpleName();
	private Rectangle area = new Rectangle();
	
	
	private void expandArea(Point point){
		float x = point.getX();
		float y = point.getY();
		
		expandArea(x, y);
	}
	
	private void expandArea(float x, float y){
		if(x < area.left)
			area.left = x;
		else if(x > area.right)
			area.right = x;
		if(y < area.top)
			area.top = y;
		else if(y > area.bottom)
			area.bottom = y;
	}
	
	public Rectangle getSelectedArea(){
		return area;
	}
	
	@Override
	public void visitLine(Line line) {
		if(area.isEmpty()){
			area = SelectionHandler.getInclusiveRect(line.getStart(), line.getEnd());
			return;
		}
		expandArea(line.getStart());
		expandArea(line.getEnd());
	}

	@Override
	public void visitCurve(Curve curve) {
		LinkedList<Point> points = curve.getPoints();
		int i = 0;
		if(area.isEmpty()){
			area = SelectionHandler.getInclusiveRect(points.get(0), points.get(1));
			i = 2;
		}
		
		for(; i < points.size(); i++){
			Point point = points.get(i);
			expandArea(point);
		}
	}

	@Override
	public void visitSquare(Square square) {
		Point start = square.getStart();
		Point end = square.getEnd();
		if(area.isEmpty()){
			area = SelectionHandler.getInclusiveRect(start, end);
			return;
		}
		expandArea(start);
		expandArea(end);
	}


	@Override
	public void visitImage(Image image) {
		Rectangle rect = image.getRect();
		Log.d(TAG, "Image rect: " + rect);
		if(area.isEmpty()){
			area = new Rectangle(rect.left, rect.top, rect.right, rect.bottom);
			return;
		}
		expandArea(image.getPosition());
		expandArea(rect.right, rect.bottom);
	}

	@Override
	public void visitEllipse(Ellipse square) {
		if(area.isEmpty()){
			area = SelectionHandler.getInclusiveRect(square.getStart(), square.getEnd());
			return;
		}
		expandArea(square.getStart());
		expandArea(square.getEnd());
	}

}

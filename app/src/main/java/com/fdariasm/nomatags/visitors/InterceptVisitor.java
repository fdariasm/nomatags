package com.fdariasm.nomatags.visitors;

import android.util.Log;

import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.models.components.Square;

import java.util.LinkedList;

public class InterceptVisitor implements ShapeVisitor{

	private static final String TAG = null;

	private Rectangle selection;
		
	private ShapeVisitor visitor;
	
	public InterceptVisitor(Rectangle selection, ShapeVisitor visitor) {
		this.selection = selection;
		this.visitor = visitor;  
	}
	
		
	@Override
	public void visitLine(Line line) {
		Point start = line.getStart();
		Point end = line.getEnd();
		Log.d(TAG, "Intercepting line: " + selection);
		if(SegmentIntersectRectangle(
				selection.left, selection.top, selection.right, selection.bottom, 
				start.getX(), start.getY(), end.getX(), end.getY())){
			Log.d(TAG, "Intercepting  accepting line");
			visitor.visitLine(line);
		}	
	}

	@Override
	public void visitCurve(Curve curve) {
		LinkedList<Point> points = curve.getPoints();
		
		for(Point point : points)
			if (selection.contains(point.getX(), point.getY())) {
				visitor.visitCurve(curve);
				return;
			}
	}


	@Override
	public void visitSquare(Square square) {
		Point start = square.getStart();
		Point end = square.getEnd();
		
		Rectangle squareRect = new Rectangle(start.getX(), start.getY(), end.getX(), end.getY());
		
		Log.d("SelectVisitor", "Before test");
		
		if(!squareRect.contains(selection) &&
				selection.intersect(squareRect)){
			visitor.visitSquare(square);
		}
	}

	@Override
	public void visitImage(Image image) {
		Rectangle rect = image.getRect();
		if(selection.intersect(rect)){
			Log.d(TAG, "Intercepting image");
			visitor.visitImage(image);
		}
	}

	@Override
	public void visitEllipse(Ellipse ellipse) {
		Point start = ellipse.getStart();
		Point end = ellipse.getEnd();
		Log.d(TAG, "Intercepting line: " + selection);
		if(SegmentIntersectRectangle(
				selection.left, selection.top, selection.right, selection.bottom,
				start.getX(), start.getY(), end.getX(), end.getY())){
			Log.d(TAG, "Intercepting  accepting line");
			visitor.visitEllipse(ellipse);
		}
	}

	//Reference: http://stackoverflow.com/a/100165/1502842
	boolean SegmentIntersectRectangle(double a_rectangleMinX,
			double a_rectangleMinY,
			double a_rectangleMaxX,
			double a_rectangleMaxY,
			double a_p1x,
			double a_p1y,
			double a_p2x,
			double a_p2y)
	{
		// Find min and max X for the segment

		double minX = a_p1x;
		double maxX = a_p2x;

		if(a_p1x > a_p2x)
		{
			minX = a_p2x;
			maxX = a_p1x;
		}

		// Find the intersection of the segment's and rectangle's x-projections

		if(maxX > a_rectangleMaxX)
		{
			maxX = a_rectangleMaxX;
		}

		if(minX < a_rectangleMinX)
		{
			minX = a_rectangleMinX;
		}

		if(minX > maxX) // If their projections do not intersect return false
		{
			return false;
		}

		// Find corresponding min and max Y for min and max X we found before

		double minY = a_p1y;
		double maxY = a_p2y;

		double dx = a_p2x - a_p1x;

		if(Math.abs(dx) > 0.0000001)
		{
			double a = (a_p2y - a_p1y) / dx;
			double b = a_p1y - a * a_p1x;
			minY = a * minX + b;
			maxY = a * maxX + b;
		}

		if(minY > maxY)
		{
			double tmp = maxY;
			maxY = minY;
			minY = tmp;
		}

		// Find the intersection of the segment's and rectangle's y-projections

		if(maxY > a_rectangleMaxY)
		{
			maxY = a_rectangleMaxY;
		}

		if(minY < a_rectangleMinY)
		{
			minY = a_rectangleMinY;
		}

		if(minY > maxY) // If Y-projections do not intersect return false
		{
			return false;
		}

		return true;
	}

}

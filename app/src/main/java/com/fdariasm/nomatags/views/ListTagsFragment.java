package com.fdariasm.nomatags.views;

import android.app.Activity;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.fdariasm.nomatags.R;
import com.fdariasm.nomatags.models.components.Tag;
import com.fdariasm.nomatags.models.process.SaveDocument;
import com.fdariasm.nomatags.models.process.TagsCursorAdapter;

/**
 * Created by fdariasm on 05/11/2015.
 */
public class ListTagsFragment extends ListFragment implements
        SaveDocument.OnCursorResultsListener, SaveDocument.OnTagsListener {

    private SaveDocument saveDocument;

    private OnTagSelected listener;

    private String nameFilter = "";

    private static final String TAG = ListTagsFragment.class.getCanonicalName();

    private int currentSelection = -1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        saveDocument = SaveDocument.getInstance(getActivity());
        return inflater.inflate(R.layout.list_tags, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //getListView().setSelector(R.drawable.tag_selector);
        ListView listView = getListView();
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listView.setSelector(android.R.color.holo_blue_light);
        registerForContextMenu(listView);
    }

    public void filterByName(String nameFilter){
        this.nameFilter = nameFilter;
        saveDocument.listTags(nameFilter, this);
    }

    @Override
    public void onResume() {
        saveDocument.listTags(nameFilter, this);
        super.onResume();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        currentSelection = position;
        Object tag = v.getTag();
        if(null != listener){
            listener.tagSelected((Integer)tag);
        }
    }

    @Override
    public void showResults(Cursor results) {
        TagsCursorAdapter adapter;
        String[] columns = {SaveDocument.TagsTable.COLUMN_NAME_NAME};
        int[] resources = {R.id.tag_name};
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            adapter = new TagsCursorAdapter(getActivity(), R.layout.tag_row,
                    results, columns, resources, 0);
        }else{
            adapter = new TagsCursorAdapter(getActivity(), R.layout.tag_row,
                    results, columns, resources);
        }

        setListAdapter(adapter);

        if(currentSelection >= 0) {
            android.util.Log.d(TAG, "Current selection: " + currentSelection);
            getListView().requestFocusFromTouch();
            getListView().setSelection(currentSelection);

        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(!(activity instanceof OnTagSelected)){
            throw new RuntimeException("Activity not instance of OnTagSelected");
        }

        listener = (OnTagSelected) activity;
    }

    @Override
    public void tagAdded(Tag tag) {

    }

    @Override
    public void tagDeleted() {
        currentSelection = -1;
        saveDocument.listTags(nameFilter, this);
        listener.tagDeleted();

        getListView().clearChoices();
        getListView().requestLayout();
    }

    public interface OnTagSelected{
        void tagSelected(int idTag);
        void tagDeleted();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        android.util.Log.i(TAG, "onCreateContextMenu");
        if (v.getId()== android.R.id.list) {
            android.util.Log.i(TAG, "Creating context menu");
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            View tv = info.targetView.findViewById(R.id.tag_name);
            menu.setHeaderTitle(((TextView) tv).getText());
            menu.add(Menu.NONE, 1, 1, getString(R.string.delete_tag));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        android.util.Log.i(TAG, "Deleting tag: " + info.targetView.getTag());
        saveDocument.deleteTag((Integer)info.targetView.getTag(), this);

        return true;
    }
}

package com.fdariasm.nomatags.views.drawing;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.text.TextPaint;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Current;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.ToolState;
import com.fdariasm.nomatags.models.VisibleShapes;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.strategies.android.ResourceLocator;
import com.fdariasm.nomatags.strategies.android.ScrollbarsDrawing;
import com.fdariasm.nomatags.visitors.DrawVisitor;

public class DrawingHelper {

	private static final String TAG = DrawingHelper.class.getSimpleName();
	private AppState appState;
	private Presentation mPresentation;
	private ResourceLocator mLocator;
	private DrawVisitor drawVisitor;
	private DocumentShapes mDocument;
	private ScrollbarsDrawing toolbarDrawing;
	private VisibleShapes visibleShapes;
	private Current current;
	
	private Paint mPaint;
	
	private Paint mBorderPaint;
	private Paint mPaintLimiter;
	private DashPathEffect mDashLimiter;
	private Paint mGenPaint;
	private TextPaint mTextPaint;
	private float tpDescent;
	private float tpAscent;
	
	private BackgroundDrawing backgroundDrawing;

	private ToolState toolState;


	public DrawingHelper(AppState state, ResourceLocator locator, VisibleShapes visibleShapes) {
		this.appState = state;
		this.visibleShapes = visibleShapes;
		mLocator = locator;
		
		mDocument = appState.getDocument();
		current = appState.getCurrent();
		mPresentation = appState.getPresentation();

		toolState = appState.getToolState();
		
		mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(toolState.getPaintColor());
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(toolState.getPaintStrokeWidth());
        
        mBorderPaint = new Paint();
		mBorderPaint.setARGB(255, 255, 0,0);
		mBorderPaint.setStyle(Style.STROKE);
		mBorderPaint.setStrokeWidth(3);
		mBorderPaint.setPathEffect(new DashPathEffect(new float[] {10,20}, 0));
		
        mPaintLimiter = new Paint();
        mPaintLimiter.setColor(Color.BLACK);
        mPaintLimiter.setStyle(Paint.Style.FILL);
        mPaintLimiter.setAntiAlias(false);
		mPaintLimiter.setDither(false);
		mPaintLimiter.setFilterBitmap(false);
		mDashLimiter = new DashPathEffect(new float[] {7,10}, 0);
		
		mGenPaint = new Paint();
		mGenPaint.setColor(Color.BLUE);
		mGenPaint.setStrokeWidth(3);
		mGenPaint.setStyle(Paint.Style.STROKE);

		mTextPaint = new TextPaint();
		mTextPaint.setColor(Color.BLUE);
		mTextPaint.setTextAlign(Align.CENTER);
		mTextPaint.setStyle(Paint.Style.STROKE);
		mTextPaint.setStrokeWidth(3);
		mTextPaint.setTextSize(30);
		mTextPaint.setAntiAlias(true);
		
		
		tpDescent = mTextPaint.descent();
		tpAscent = mTextPaint.ascent();
		
        mLocator.installPaint(mPaint);
        
        drawVisitor = new DrawVisitor(mLocator, mPresentation);
        
        toolbarDrawing = new ScrollbarsDrawing(mPresentation);
        
        backgroundDrawing = new BackgroundDrawing(mPresentation, mPaintLimiter);
	}
	
	public void doDraw(Canvas canvas){
		updateDocument(canvas);
	}

	private void updateDocument(Canvas canvas) {
		if(null == canvas) return;

		canvas.drawColor(0xFFFFFFFF);

		float vertOffset = mPresentation.getVertOffset();
		float hOffset = mPresentation.getHOffset();
		float scale = mPresentation.getScale();
		int pageNumber = mPresentation.getCurrentPageNumber();
		float currLimit = mPresentation.getLimit(pageNumber);
		ShapeSelection currentSelection = mDocument.getCurrentSelection();
		
		drawLimiter( canvas, pageNumber, currLimit * scale );
		int preScale = canvas.save();
		canvas.scale(scale, scale);
		
		backgroundDrawing.drawRowPattern(canvas, vertOffset, hOffset, scale);
		
		int preTrans = canvas.save();
		canvas.translate(-hOffset, -vertOffset);
		
		mLocator.installCanvas(canvas);

		visibleShapes.accept(drawVisitor);

		current.accept(drawVisitor);

		drawSelection(canvas, currentSelection);
		
		canvas.restoreToCount(preTrans);
//		drawPatternBackground(canvas, vertOffset, hOffset, scale);
		canvas.restoreToCount(preScale);
		
		drawAreaSelection(canvas, currentSelection, vertOffset, hOffset,  scale);

//		drawLimiter( canvas, pageNumber, currLimit * scale );
		toolbarDrawing.drawScrollbars(canvas);
		
//		long end = System.currentTimeMillis();
//		Log.i(TAG, "Document Time: " + (end-start) );
	}
	
	private void drawLimiter(Canvas canvas,
			int pageNumber, float currLim){
		android.util.Log.i(TAG, "Page number: " + pageNumber);;
		if(currLim > 0 && currLim < (mPresentation.getScreenHeight() )){
			canvas.drawText("Page " + pageNumber, 20, currLim - 20, mPaintLimiter);
			mPaintLimiter.setPathEffect(mDashLimiter);
			canvas.drawLine(0, currLim, mPresentation.getScreenWidth(), currLim, mPaintLimiter);
			mPaintLimiter.setPathEffect(null);
			canvas.drawText("Page " + (pageNumber + 1), 20, currLim + 30, mPaintLimiter);
		}
	}
	
	private void drawSelection(Canvas canvas, ShapeSelection currentSelection) {
		Rectangle userSelection = current.getUserSelection();
		if(!userSelection.isEmpty()){
			canvas.drawRect(convert(userSelection), this.mBorderPaint);
		}
		if(!currentSelection.isEmpty()){
			canvas.drawRect(convert(currentSelection.getRect()), this.mBorderPaint);
		}
	}
	
	private void drawAreaSelection(Canvas canvas, 
			ShapeSelection currentSelection, float vOffset, float hOffset, float scale){
		if(!currentSelection.isEmpty()){
			Point transformPoint = currentSelection.getTransformPoint();
			float x = (transformPoint.getX() - hOffset) * scale;
			float y = (transformPoint.getY() - vOffset) * scale;
			RectF sel = new RectF(x - (Presentation.TRANSFORMAREA * 1.3f), y - Presentation.TRANSFORMAREA, 
					x + (Presentation.TRANSFORMAREA * 1.3f), y + Presentation.TRANSFORMAREA);
			canvas.drawText("" + currentSelection.getShapes().size(), x,  
					y - ((tpDescent + tpAscent) / 2), mTextPaint);
			canvas.drawOval(sel, mTextPaint); 
		}
	}


	
	private RectF convert(Rectangle user){
		float strokeWidth = toolState.getPaintStrokeWidth();
		return new RectF(user.left - strokeWidth, user.top - strokeWidth, 
				user.right + strokeWidth, user.bottom + strokeWidth);
	}
	
}

package com.fdariasm.nomatags.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.fdariasm.nomatags.R;
import com.fdariasm.nomatags.controllers.TouchEventsController;
import com.fdariasm.nomatags.events.DocChangedEvent;
import com.fdariasm.nomatags.events.PresentationEvent;
import com.fdariasm.nomatags.listeners.DocumentListener;
import com.fdariasm.nomatags.listeners.PresentationListener;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.VisibleShapes;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.strategies.android.ResourceLocator;
import com.fdariasm.nomatags.views.drawing.DrawingHelper;
import com.fdariasm.nomatags.views.drawing.UpdateArea;

public class CanvasSurfaceView extends SurfaceView implements 
										SurfaceHolder.Callback, 
										DocumentListener, PresentationListener{
	
	private TouchEventsController mController;

	private DocumentShapes mDocument;
	
	private Presentation mPresentation;
	
	private ResourceLocator mLocator;
		
	private DrawingThread secThread;
			
	private Boolean requireUpdate = true;
	private final Object lock = new Object();
	private UpdateArea updateArea;
	
	private VisibleShapes visibleShapes;
	
	private AppState appState;

	private Rect totalArea;

	private SurfaceHolder holder;
	
	private DrawingHelper drawingHelper;
	
	private final static String TAG = CanvasSurfaceView.class.getCanonicalName();

	private Rectangle currentArea;

	private Paint mPaint;


	public CanvasSurfaceView(Context context) {
		super(context);
	}
	public CanvasSurfaceView(Context context, AppState state, TouchEventsController touchController) {
		super(context);
		
		this.appState = state;
		
		mDocument = state.getDocument();
		appState.addDocumentListener(this);
		
		mPresentation = state.getPresentation();
		mPresentation.addListener(this);
		
		mController = touchController;
		       
        mLocator = ResourceLocator.getLocator();
                
        holder = getHolder();
        holder.addCallback(this);
      
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFF000000);
        mPaint.setStyle(Paint.Style.STROKE);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return mController.processTouch(event);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		mPresentation.setScreenWidth(w);
		mPresentation.setScreenHeight(h);
		
		float letterWidth = getResources().getDimension(R.dimen.letter_width);
		float letterHeight = getResources().getDimension(R.dimen.letter_height);
		mPresentation.setPageMetrics(letterWidth, letterHeight);
		Log.d(TAG, String.format("Doc height: %f width: %f", letterHeight, letterWidth));
		Log.d(TAG, String.format("Screen height: %d width: %d", h, w));
		
		totalArea = new Rect(0, 0, w, h);
		
		updateArea = new UpdateArea(totalArea, mPresentation);
		
		updateVisibleArea();
	}
	
	private void updateVisibleArea() {
		
		currentArea = getVisibleArea();
		
		visibleShapes = new VisibleShapes(appState, currentArea);
		
        drawingHelper = new DrawingHelper(appState, mLocator, visibleShapes);
	}
	
	
	private Rectangle getVisibleArea() {
		int pageNumber = mPresentation.getCurrentPageNumber();
		float currentLimit = mPresentation.getCurrentLimit();
		float pageHeight = mPresentation.getPageHeight();
		float pageWidth = mPresentation.getPageWidth();
		float scale = mPresentation.getScale();
		
		if(currentLimit >= 0 && currentLimit <= (mPresentation.getScreenHeight() / scale) ){
			//Two pages at the same time
			return new Rectangle(
					0, pageHeight * (pageNumber - 1),
					pageWidth, pageHeight * (pageNumber + 1));
		}else{
			return new Rectangle(
					0, pageHeight * (pageNumber - 1),
					pageWidth, pageHeight * pageNumber);
		}
	}
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		mDocument.removeListener(this);
		mPresentation.removeListener(this);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		secThread = new DrawingThread(holder);        
		secThread.setRunning(true);
		secThread.start();
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {	
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        secThread.setRunning(false);
        while (retry) {
            try {
            	synchronized (lock) {
					lock.notify();
				}
                secThread.join();
                retry = false;
                
            } catch (InterruptedException e) {
            	Log.e(TAG, "", e);
            }
        }
	}
	@Override
	public void presentationChanged(PresentationEvent event) {
		synchronized (lock) {			
			Rectangle visibleArea = getVisibleArea();
			if(!currentArea.equals(visibleArea)){
				visibleShapes.updateVisibles(visibleArea);
				currentArea = visibleArea;
			}
			updateArea.reset(totalArea);

			if(!requireUpdate){
				requireUpdate = true;
				lock.notify();
			}
		}
	}
	
	@Override
	public void documentChanged(DocChangedEvent event) {
		synchronized ( lock ) {
			Point changed = event.getChangedPoint();
			switch(event.getType()){
			case NEW:
				updateArea = new UpdateArea(null, changed, this.appState);
				break;
			case CHANGENEW:
				if(updateArea.isUpdated()){
					updateArea.reset(event.getStartChange(), event.getEndChange());
				}else{
					updateArea.expandArea(event.getEndChange() );
				}
				break;
			case ADD:
				visibleShapes.add(event.getShape());
				updateArea.reset(totalArea);
				break;
			case REMOVE:
				visibleShapes.remove(event.getShape());
				updateArea.reset(totalArea);
				break;
			case COPY:
				visibleShapes.updateVisibles(getVisibleArea());
				updateArea.reset(totalArea);
				break;
			default:
				updateArea.reset(totalArea);
			}			
			if(!requireUpdate){
				requireUpdate = true;
				lock.notify();
			}
		}
	}
	
	private class DrawingThread extends Thread{
		
		private volatile boolean mRun = false;
		private SurfaceHolder mSurfaceHolder;
		
		public DrawingThread(SurfaceHolder surfaceHolder) {
			mSurfaceHolder = surfaceHolder;
		}
		
		public void setRunning(boolean run){
			mRun = run;
		}
		
		@Override
		public void run() {
			while(mRun){
				Canvas canvas = null;
				try{
					Rect area = updateArea.getArea();
					synchronized(lock){
						requireUpdate = false;
					}
					canvas = mSurfaceHolder.lockCanvas(area);
					synchronized(mSurfaceHolder){
						drawingHelper.doDraw(canvas);
//						canvas.drawRect(area, mPaint);//FIXME
					}
				}finally{
					if(null != canvas)
						mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
				synchronized(lock){
					try {
						if(!requireUpdate){
							lock.wait();
						}
					} catch (InterruptedException e) {
						android.util.Log.e(TAG, "", e);
					}
				}
			}
		}
	}
}

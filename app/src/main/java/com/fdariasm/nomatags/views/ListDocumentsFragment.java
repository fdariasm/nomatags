package com.fdariasm.nomatags.views;

        import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fdariasm.nomatags.EditActivity;
import com.fdariasm.nomatags.R;
import com.fdariasm.nomatags.models.process.DocumentsCursorAdapter;
import com.fdariasm.nomatags.models.process.SaveDocument;

/**
 * Created by fdariasm on 05/11/2015.
 */
public class ListDocumentsFragment extends ListFragment implements
        SaveDocument.OnCursorResultsListener {

    private SaveDocument saveDocument;

    private CurrentSearch defaultSearch = new CurrentSearch() {
        @Override
        public void initSearch() {
            saveDocument.listDocuments(ListDocumentsFragment.this);
        }
    };
    private CurrentSearch currentSearch = defaultSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        saveDocument = SaveDocument.getInstance(getActivity());
        return inflater.inflate(R.layout.list_documents, container, false);
    }

    public void filterByName(final String name){
        currentSearch = new CurrentSearch() {
            @Override
            public void initSearch() {
                saveDocument.listDocumentsByName(ListDocumentsFragment.this, name);
            }
        };
        currentSearch.initSearch();
    }

    public void filterByTag(final int idTag){
        currentSearch = new CurrentSearch() {
            @Override
            public void initSearch() {
                saveDocument.listDocumentsByTag(ListDocumentsFragment.this, idTag);
            }
        };
        currentSearch.initSearch();
    }

    public void resetFilter(){
        currentSearch = defaultSearch;
        currentSearch.initSearch();
    }


    @Override
    public void onResume() {
        currentSearch.initSearch();
        super.onResume();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Object tag = v.getTag();
        loadDocument((Integer) tag);
    }

    private void loadDocument(int id) {
        Intent i = new Intent();
        Bundle b = new Bundle();
        b.putSerializable(EditActivity.KEY_DOCUMENT, id);
        i.putExtras(b);
        i.setClass(getActivity(), EditActivity.class);
        startActivity(i);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressWarnings("deprecation")
    @Override
    public void showResults(Cursor results) {
        DocumentsCursorAdapter adapter;
        String[] columns = {SaveDocument.DocumentTable.COLUMN_NAME_NAME, SaveDocument.DocumentTable.COLUMN_NAME_CREATION_DATE};
        int[] resources = {R.id.doc_name, R.id.doc_date};
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            adapter = new DocumentsCursorAdapter(getActivity(), R.layout.results_row,
                    results, columns, resources, 0);
        }else{
            adapter = new DocumentsCursorAdapter(getActivity(), R.layout.results_row,
                    results, columns, resources);
        }

        setListAdapter(adapter);
    }

    private interface CurrentSearch{
        void initSearch();
    }
}

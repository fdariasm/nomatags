package com.fdariasm.nomatags.views.drawing;

import com.fdariasm.nomatags.models.Presentation;

import android.graphics.Canvas;
import android.graphics.Paint;

public class BackgroundDrawing {

	private float gridSize;
	private Presentation mPresentation;
	private Paint mPaintLimiter;
	private float rowSize;
	
	public BackgroundDrawing(Presentation mPresentation, Paint mPaintLimiter) {
		this.mPresentation = mPresentation;
		this.mPaintLimiter = mPaintLimiter;
		
		gridSize = Presentation.GRIDSIZE;
		rowSize = Presentation.ROWSIZE;
	}

	public void drawGridPattern(Canvas canvas, 
			float vertOffset, float hOffset, float scale){
		
		float screenHeight = mPresentation.getScreenHeight() / scale;
		float screenWidth = mPresentation.getScreenWidth() / scale;
		
		for(int i = 0; i < vertOffset + screenHeight; i += gridSize){
			float pos = i - vertOffset;
			if(pos < 0 || pos > screenHeight) continue;
			canvas.drawLine(0, pos, screenWidth, pos, mPaintLimiter);
		}
		//Vertical Lines
		for(float i = 0; i < hOffset + screenWidth; i += gridSize){
			float pos = i - hOffset;
			if(pos < 0 || pos > screenWidth) continue;
			canvas.drawLine(pos, 0, pos, screenHeight, mPaintLimiter);
		}
	}
	
	public void drawRowPattern(Canvas canvas, 
			float vertOffset, float hOffset, float scale){
		
		float screenHeight = mPresentation.getScreenHeight() / scale;
		float screenWidth = mPresentation.getScreenWidth() / scale;
		int init = (int) (rowSize + Presentation.ROW_MARGIN);
		for(int i = init; i < vertOffset + screenHeight; i += rowSize){
			float pos = i - vertOffset;
			if(pos < 0 || pos > screenHeight) continue;
			canvas.drawLine(Presentation.ROW_MARGIN / scale, pos, 
					screenWidth - (Presentation.ROW_MARGIN/scale), pos, 
					mPaintLimiter);
		}
	}
}

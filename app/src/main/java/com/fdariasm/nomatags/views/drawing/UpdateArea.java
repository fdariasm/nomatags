package com.fdariasm.nomatags.views.drawing;

import android.graphics.Rect;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.ToolState;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.utils.TwoTuple;
import com.fdariasm.nomatags.visitors.DrawVisitor;

public class UpdateArea {
	
	private Rect changedArea;
	
	private boolean isUpdated = false;

	private Presentation presentation;
	
	private ToolState toolState;

	public UpdateArea(Point lastPoint, Point initial, AppState appState) {
		
		this.presentation = appState.getPresentation();
		this.toolState = appState.getToolState();
		Point start;
		Point end;
		if(null == lastPoint){
			start = end = initial;
		}else{
			TwoTuple<Point> pointsOrder = DrawVisitor.getPointsOrder(lastPoint, initial);
			start = pointsOrder.first;
			end = pointsOrder.second;
		}
		float margin = toolState.getPaintStrokeWidth() * Math.max(presentation.getScale(), 1);
		int initX = (int) (start.getX() - margin);
		int initY = (int) (start.getY() - margin);
		int endX = (int) (end.getX() + margin);
		int endY = (int) (end.getY() + margin );
		
		changedArea = new Rect(initX, initY, endX, endY);
	}
	
	public synchronized void reset(Point lastPoint, Point initial ){
		Point start;
		Point end;
		if(null == lastPoint){
			start = end = initial;
		}else{
			TwoTuple<Point> pointsOrder = DrawVisitor.getPointsOrder(lastPoint, initial);
			start = pointsOrder.first;
			end = pointsOrder.second;
		}
		
		float margin = toolState.getPaintStrokeWidth() * Math.max(presentation.getScale(), 1);
		
		int initX = (int) (start.getX() - margin);
		int initY = (int) (start.getY() - margin );
		int endX = (int) (end.getX() + margin);
		int endY = (int) (end.getY() + margin);
		
		changedArea = new Rect(initX, initY, endX, endY);
		isUpdated = false;
	}
	
	public UpdateArea(Rect rect, Presentation presentention){
		changedArea = rect;
		this.presentation = presentention;
	}
	
	public synchronized void reset(Rect rect){
		changedArea = rect;
		isUpdated = false;
	}
	
	public synchronized void expandArea(Point changed){
		int x = (int) changed.getX();
		int y = (int) (changed.getY());
		int margin = (int) (toolState.getPaintStrokeWidth() * Math.max(presentation.getScale(), 1));
		
		if(x < changedArea.left)
			changedArea.left = x - margin;
		if(x > changedArea.right)
			changedArea.right = x + margin;
		if(y < changedArea.top)
			changedArea.top = y - margin;
		if(y > changedArea.bottom)
			changedArea.bottom = y + margin;
	}
	
	public synchronized Rect getArea(){
		isUpdated = true;
		return changedArea;
	}

	public synchronized boolean isUpdated() {
		return isUpdated;
	}

}

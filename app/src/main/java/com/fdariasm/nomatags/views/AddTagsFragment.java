package com.fdariasm.nomatags.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;

import com.fdariasm.nomatags.R;
import com.fdariasm.nomatags.models.components.Tag;
import com.fdariasm.nomatags.models.process.SaveDocument;
import com.fdariasm.nomatags.models.process.TagsAdapter;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by fdariasm on 04/11/2015.
 */
public class AddTagsFragment extends ListFragment {

    public static final String TAGS_KEY = "TAGS";

    private TagsAdapter adapter;
    private HashSet<Tag> tags;

    private static final String TAG = AddTagsFragment.class.getCanonicalName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.tag_activity, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();

        if(null == extras) throw new RuntimeException("Missing bundle");

        tags = (HashSet<Tag>) extras.getSerializable(TAGS_KEY);

        initTags(tags);
        registerForContextMenu(getListView());
    }

    private void initTags(HashSet<Tag> tags) {
        adapter = new TagsAdapter(getActivity(), new ArrayList<Tag>(tags));
        setListAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        android.util.Log.i(TAG, "onCreateContextMenu");
        if (v.getId()== android.R.id.list) {
            android.util.Log.i(TAG, "Creating context menu");
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Tag tag = adapter.getItem(info.position);
            menu.setHeaderTitle(tag.getName());
            menu.add(Menu.NONE, 1,1, getString(R.string.delete_tag));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        Tag tag = adapter.getItem(info.position);
        tags.remove(tag);

        initTags(tags);
        return true;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tags_options, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.new_tag:
                newTag();
                return true;
            case R.id.accept_tag:
                acceptTags();
                return true;
        }
        return true;
    }

    private void newTag() {
        LayoutInflater li = LayoutInflater.from(getActivity());

        View promptsView = li.inflate(R.layout.add_tag, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.add_tag_input);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String tagName = userInput.getText().toString();
                                if (null != tagName && !tagName.isEmpty()) {
                                    SaveDocument sd = SaveDocument.getInstance(getActivity());
                                    sd.loadTag(tagName, tagsListener);
                                }
                            }
                        })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        //taken from: http://www.mkyong.com/android/android-prompt-user-input-dialog-example/
    }

    private void acceptTags() {
        if(null == tags){
            getActivity().finish();
            return;
        }

        Intent returnIntent = new Intent();
        Bundle b = new Bundle();
        b.putSerializable(TAGS_KEY, tags);
        returnIntent.putExtras(b);
        getActivity().setResult(Activity.RESULT_OK, returnIntent);
        getActivity().finish();
    }

    private SaveDocument.OnTagsListener tagsListener = new SaveDocument.OnTagsListener() {
        @Override
        public void tagAdded(Tag tag) {
            android.util.Log.e("Listener", "Tag added: " + tag.getId());
            if(tags.add(tag))
                initTags(tags);
        }

        @Override
        public void tagDeleted() {
        }
    };
}

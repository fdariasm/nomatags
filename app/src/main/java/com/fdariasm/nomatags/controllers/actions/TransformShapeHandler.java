package com.fdariasm.nomatags.controllers.actions;

import android.util.Log;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.actions.TransformAction;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.visitors.AreaSelectionVisitor;
import com.fdariasm.nomatags.visitors.RotateShapeVisitor;
import com.fdariasm.nomatags.visitors.ScaleShapeVisitor;

public class TransformShapeHandler extends ActionHandler{

	
	
	static final String TAG = TransformShapeHandler.class.getSimpleName();
	private Point initPoint;
	private Point centerPoint;
	private float prevDistance;
	private float totalFactor;
	private ShapeSelection currentSelection;
	private float prevX;
	private float prevY;
	private double totalAngle;

	public TransformShapeHandler(AppState appState) {
		super(appState);
	}

	@Override
	public void actionDown(DrawEvent event) {
		
		centerPoint =  getSelectionCenter();
		initPoint = event.getDocumentPoint();
		prevDistance = centerPoint.distanceTo(initPoint);
		
		totalFactor = 1.f;
		totalAngle = 0;
		
		prevX = initPoint.getX() - centerPoint.getX();
		prevY = initPoint.getY() - centerPoint.getY();		
	}

	private Point getSelectionCenter() {
		
		currentSelection = mDocument.getCurrentSelection();
		Rectangle rect = currentSelection.getRect();
		
		return new Point(
				(rect.right + rect.left)/2,
				(rect.bottom + rect.top)/2);
	}

	@Override
	public void actionMove(DrawEvent event) {
		Point documentPoint = event.getDocumentPoint();
		
		float scaleFactor = getScale(documentPoint);
		totalFactor *= scaleFactor;
		
		double angle = getAngle(documentPoint);
		
		totalAngle += angle;
		
		doTransform(scaleFactor, angle);
		
		currentSelection.setTransformPoint(documentPoint);
	}

	private double getAngle(Point documentPoint) {
		float currX = documentPoint.getX() - centerPoint.getX();
		float currY = documentPoint.getY() - centerPoint.getY();
		
		double angle = Math.atan2(currY, currX) - Math.atan2(prevY, prevX);
		prevX = currX;
		prevY = currY;
		Log.d(TAG, "Angle: " + angle);
		return angle;
	}

	private float getScale(Point eventPoint) {
		float currentDistance = centerPoint.distanceTo(eventPoint);
		
		float scaleFactor = currentDistance / prevDistance;
		prevDistance = currentDistance;
		
		return scaleFactor;
	}
	
	private void doTransform(float scaleFactor, double angle){
		ScaleShapeVisitor scaleVisitor = new ScaleShapeVisitor(scaleFactor, centerPoint);
		RotateShapeVisitor rotateVisitor = new RotateShapeVisitor((float) angle, centerPoint);
		AreaSelectionVisitor areaVisitor = new AreaSelectionVisitor();
		for(BaseShape shape : currentSelection.getShapes()){
			shape.accept(scaleVisitor);
			shape.accept(rotateVisitor);
			shape.accept(areaVisitor);
		}
		currentSelection.setRect(areaVisitor.getSelectedArea());
	}

	@Override
	public void actionUp(DrawEvent event) {
		Point documentPoint = event.getDocumentPoint();
		
		float scaleFactor = getScale(documentPoint);
		totalFactor *= scaleFactor;
		
		double angle = getAngle(documentPoint);
		totalAngle += angle;
		
		doTransform(scaleFactor, angle);
		
		Rectangle rect = currentSelection.getRect();
		Point np = new Point((rect.left + rect.right)/2, rect.bottom + Presentation.TRANSFORMDISTANCE);
		currentSelection.setTransformPoint(np);
		
		TransformAction scaleAction = new TransformAction(appState, centerPoint, totalFactor, (float) totalAngle);
		addAction(scaleAction);
	}

	@Override
	public void actionCancel(DrawEvent event) {
		doTransform(1 / totalFactor, -totalAngle);
	}
}

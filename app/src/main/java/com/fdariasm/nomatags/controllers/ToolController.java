package com.fdariasm.nomatags.controllers;

import android.util.Log;

import com.fdariasm.nomatags.controllers.actions.ActionHandler;
import com.fdariasm.nomatags.controllers.actions.DrawCurveHandler;
import com.fdariasm.nomatags.controllers.actions.DrawEllipseHandler;
import com.fdariasm.nomatags.controllers.actions.DrawLineHandler;
import com.fdariasm.nomatags.controllers.actions.DrawSquareHandler;
import com.fdariasm.nomatags.controllers.actions.HorScrollHandler;
import com.fdariasm.nomatags.controllers.actions.SelectTransformHandler;
import com.fdariasm.nomatags.controllers.actions.VertScrollHandler;
import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.events.ToolChangedEvent;
import com.fdariasm.nomatags.listeners.ToolChangedListener;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.strategies.android.AndroidStrategyFactory;

/*
 * State pattern
 */
public class ToolController implements ToolChangedListener {
	
	private static final String TAG = ToolController.class.getSimpleName();

	private VertScrollHandler scrollbarHandler;
	
	private ActionHandler mActionHandler;
		
	private Presentation presentation;
	
	private ActionHandler defHandler;

	private AppState appState;

	private HorScrollHandler hScrollbarHandler;

	public ToolController(AppState appState) {
		
		this.appState = appState;
		
		appState.addToolListener(this);
				
		presentation = appState.getPresentation();


		scrollbarHandler = new VertScrollHandler(appState);
		hScrollbarHandler = new HorScrollHandler(appState);
		
		//defHandler = mActionHandler;
	}
	
	public ActionHandler getHandler(DrawEvent event){
		float x = event.getX();
		float y = event.getY(); 
		switch(event.getType()){
		case DOWN:
			if(isInVertBar(x, y))
				defHandler = scrollbarHandler;
			else if(isInHBar(x, y)){
				defHandler = hScrollbarHandler;
				Log.d(TAG, "Is in h bar");
			}else
				defHandler = mActionHandler;
			break;
		default:
			break;
		}
		
		return defHandler;
	}
	
	private boolean isInVertBar(float x, float y){
		
		int screenWidth = presentation.getScreenWidth();
		float vertOffset = presentation.getScrollVertOffset();
		
		return (x >= (screenWidth - Presentation.VSCROLLWIDTH) &&  
				(x <= (screenWidth - Presentation.MARGIN)))
				&& (y >= (vertOffset) &&  (y <= (vertOffset + Presentation.VSCROLLHEIGHT)));
	}
	
	private boolean isInHBar(float x, float y){
		
		int screenHeight = presentation.getScreenHeight();
		float hOffset = presentation.getScrollHOffset();
		
		return (presentation.getHMultiplier() > 0.f) && (y >= (screenHeight - Presentation.HSCROLLWIDTH) &&  
				(y <= (screenHeight - Presentation.MARGIN)))
				&& (x >= (hOffset) &&  (x <= (hOffset + Presentation.HSCROLLWIDTH)));
	}
	

	@Override
	public void toolChanged(ToolChangedEvent event) {
		processEvent(event);

	}
	
	private void processEvent(ToolChangedEvent event){
		switch(event.selected){
		case LINE:
			mActionHandler = new DrawLineHandler(appState);
			break;
		case SQUARE:
			mActionHandler = new DrawSquareHandler(appState);
			break;
		case CURVE:
			mActionHandler = new DrawCurveHandler(appState,
					AndroidStrategyFactory.getFactory());
			break;
        case ELLIPSE:
            mActionHandler = new DrawEllipseHandler(appState);
            break;
		case SELECT:
			mActionHandler = new SelectTransformHandler(appState);
			break;
		}

	}
}

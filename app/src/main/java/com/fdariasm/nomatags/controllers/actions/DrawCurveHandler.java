package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;
import com.fdariasm.nomatags.strategies.DrawStrategyFactory;

public class DrawCurveHandler extends DrawHandler{

	protected Curve mCurve;
	protected DrawShapeStrategy mStrategy;
	
	protected DrawStrategyFactory mFactory;
	
	protected Point p1;
    protected Point p0;
		
	protected static final float TOUCH_TOLERANCE = 3;
	
	public DrawCurveHandler(AppState document, 
			DrawStrategyFactory factory) {
		super(document);
		mFactory = factory;
	}

	@Override
	public void actionDown(DrawEvent event) {
		Point point = event.getDocumentPoint();
		doDown(point, event.getPoint());
		mCurrent.currentShape(mCurve, point);
	}
	
	protected void doDown(Point docPoint, Point changedPoint){
		p0 = p1 = docPoint;
		mStrategy = mFactory.getDrawStrategy();
		mStrategy.moveTo(docPoint);
		mCurve = shapeFactory.createCurve(mStrategy);
		mCurve.addPoint(docPoint);
		
		startChangedPoint = endChangedPoint = changedPoint;
		
	}

	@Override
	public void actionMove(DrawEvent event) {		
		doMove(event.getDocumentPoint(), event.getPoint());
	}
	
	protected void doMove(Point docPoint, Point changedPoint){
		move(docPoint);
		mCurrent.areaChanged(startChangedPoint, endChangedPoint);
		startChangedPoint = endChangedPoint;
		endChangedPoint = changedPoint;
	}
	
	protected void move(Point current){
		if(pointInTolerance(current)){
			if(null != p0){
				cubicSpline(current, false);
			}
			p0 = p1;
			p1 = current;
			mCurve.addPoint(current);
		}
	}

	@Override
	public void actionUp(DrawEvent event) {
		this.mCurrent.clear();
		
		doUp(event.getDocumentPoint());
				
		mDocument.add(mCurve);
		createAction(mCurve);
	}
	
	protected void doUp(Point docPoint){
		
		if(docPoint.equals(p0)){
			Point singlePoint = getSinglePoint(docPoint);
			mStrategy.lineTo(singlePoint);
			mCurve.addPoint(singlePoint);

			return;
		}
		cubicSpline(docPoint, true);
		mCurve.addPoint(docPoint);
	}
	
	protected void cubicSpline(Point p2, boolean lastPoint){
		
    	float x = (2*p0.getX() + p1.getX())/3;
    	float y = (2*p0.getY() + p1.getY())/3;
    	Point c1 = new Point(x, y);
    	
    	x = (p0.getX() + 2*p1.getX())/3;
    	y = (p0.getY() + 2*p1.getY())/3;
    	Point c2 = new Point(x, y);
    	
    	x = (2*p1.getX() + p2.getX())/3;
    	y = (2*p1.getY() + p2.getY())/3;
    	Point c3 = new Point(x, y);
    	
    	x = (p1.getX() + 2*p2.getX())/3;
    	y = (p1.getY() + 2*p2.getY())/3;
    	Point c4 = new Point(x, y);
    	
    	x = (c2.getX() + c3.getX())/2;
    	y = (c2.getY() + c3.getY())/2;
    	Point s1 = new Point(x, y);
    	

    	mStrategy.cubicTo(c1, c2, s1);
    	    	
    	if(lastPoint){
    		mStrategy.cubicTo(c3, c4, p2);
    	}
	}
	
	protected boolean pointInTolerance(Point p){
		float dx = Math.abs(p.getX() - p1.getX());
		float dy = Math.abs(p.getY() - p1.getY());
		return dx > TOUCH_TOLERANCE || dy > TOUCH_TOLERANCE;
	}
}
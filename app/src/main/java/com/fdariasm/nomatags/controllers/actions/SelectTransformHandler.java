package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;

public class SelectTransformHandler extends ActionHandler{

	private SelectionHandler selectHandler;
	private MoveShapeHandler moveHandler;
	private TransformShapeHandler scaleHandler;
	
	private ActionHandler currentAction;
	
	public SelectTransformHandler(AppState appState) {
		super(appState);
		selectHandler = new SelectionHandler(appState);
		moveHandler = new MoveShapeHandler(appState);
		scaleHandler = new TransformShapeHandler(appState);
		currentAction = selectHandler;
				
	}

	@Override
	public void actionDown(DrawEvent event) {
		Point point = event.getDocumentPoint();
		
		currentAction = getHandler(point);
				
		currentAction.actionDown(event);
	}

	private ActionHandler getHandler(Point point) {
		ShapeSelection currentSelection = mDocument.getCurrentSelection();
		Rectangle rect = currentSelection.getRect();
		
		Rectangle transArea = getTransformArea((rect.right + rect.left) /2, 
				rect.bottom + Presentation.TRANSFORMDISTANCE);

		
		if(transArea.contains(point.getX(), point.getY())){
			return scaleHandler;
		}
		
		if(currentSelection.getRect().contains(point.getX(), point.getY())){
			return moveHandler;
		}
				
		return selectHandler;
	}
	
	private Rectangle getTransformArea(float x, float y){
		Rectangle area = new Rectangle(
				x - Presentation.TRANSFORMAREA, y - Presentation.TRANSFORMAREA,
				x + Presentation.TRANSFORMAREA, y + Presentation.TRANSFORMAREA);
		
		return area;
	}

	@Override
	public void actionMove(DrawEvent event) {
		currentAction.actionMove(event);
	}

	@Override
	public void actionUp(DrawEvent event) {
		currentAction.actionUp(event);
	}
	
	@Override
	public void actionCancel(DrawEvent event) {
		currentAction.actionCancel(event);
	}

}

package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Current;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.actions.Action;
import com.fdariasm.nomatags.models.actions.UndoManager;
import com.fdariasm.nomatags.models.components.Point;

public abstract class ActionHandler {
	
	protected AppState appState;
	
	protected DocumentShapes mDocument;
	
	protected UndoManager mManager;
	
	public static final int SINGLEPOINTSIZE = 4;

	protected Current mCurrent;
	
	public ActionHandler(AppState appState) {
		this.appState = appState;
		
		mDocument = appState.getDocument();
		
		mCurrent = appState.getCurrent();
		
		mManager = UndoManager.getManager();
		
	}
	
	public abstract void actionDown(DrawEvent event);
	
	public abstract void actionMove(DrawEvent event);
	
	public abstract void actionUp(DrawEvent event);
	
	public abstract void actionCancel(DrawEvent event);
	
	/**
	 * Helper method to add action a undoManager
	 * @param action
	 */
	protected void addAction(Action action){
		mManager.pushUndo(action);
	}
	
	/**
	 * Returns point with SINGLEPOINTSIZE added to x and y 
	 * coordinates.
	 * @param point
	 * @return
	 */
	protected Point getSinglePoint(Point point){
		float x = point.getX() + SINGLEPOINTSIZE;
		float y = point.getY() + SINGLEPOINTSIZE;
		return new Point(x, y);
	}
	
	/**
	 * Returns the current normalized point from event, where normalized means
	 * that if current point is equals to first, it adds SINGLEPOINTSIZE difference 
	 * @param event
	 * @param first Point to compare with
	 * @return normalized point if neccessary
	 */
	protected Point getCurrentNorm(DrawEvent event, Point first){
		Point current = event.getDocumentPoint();
		if(current.equals(first)){
			Point singlePoint = getSinglePoint(current);
			current = singlePoint;
		}
		return current;
	}

}

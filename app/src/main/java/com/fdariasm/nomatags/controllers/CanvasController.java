package com.fdariasm.nomatags.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fdariasm.nomatags.events.ToolChangedEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.ToolState;
import com.fdariasm.nomatags.models.actions.Action;
import com.fdariasm.nomatags.models.actions.ChangeToolAction;
import com.fdariasm.nomatags.models.actions.DeleteAction;
import com.fdariasm.nomatags.models.actions.PasteAction;
import com.fdariasm.nomatags.models.actions.UndoManager;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.models.process.ImportImage;
import com.fdariasm.nomatags.views.CanvasSurfaceView;
import com.fdariasm.nomatags.visitors.AreaSelectionVisitor;
import com.fdariasm.nomatags.visitors.CopyShapeVisitor;
import com.fdariasm.nomatags.visitors.MoveShapeVisitor;

import java.util.LinkedList;

public class CanvasController extends Fragment{

	private static final int RESULT_LOAD_IMAGE = 1;
	
	private CanvasSurfaceView mCanvasView;

	private AppState appState;
	
	private UndoManager undoManager = UndoManager.getManager();

	private ToolState toolState;

	
	private TouchEventsController touchController;
	
	public AppState getState(){
		return appState;
	}

	public CanvasController() {
		appState = new AppState();
		toolState = appState.getToolState();
		
		touchController = new TouchEventsController(appState);
	}

	public void initDocument(DocumentShapes document){
		appState = new AppState(document);
		toolState = appState.getToolState();

		touchController = new TouchEventsController(appState);
	}
		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		setRetainInstance(true);

		touchController.initGestures(getActivity());
				
		mCanvasView = new CanvasSurfaceView(getActivity(), appState, touchController);
				
		toolState.setTool(ToolChangedEvent.DrawingTool.CURVE);
		
//		touchController.setGestureDetector(
//				new ScaleGestureDetector(getActivity(), touchController.getScaleListener()));

		return mCanvasView;
	}

	
	public void deleteSelectionEvent(){
		ShapeSelection currentSelection = appState.getDocument().getCurrentSelection();
		if(!currentSelection.isEmpty()){
			DeleteAction deleteAction = new DeleteAction(appState,
					currentSelection);
			deleteAction.execute();
			undoManager.pushUndo(deleteAction);
		}
	}
	
	public void copySelectionEvent(){
		new Thread(new Runnable(){

			@Override
			public void run() {
				DocumentShapes document = appState.getDocument();
				ShapeSelection currentSelection = document.getCurrentSelection();
				if(currentSelection.isEmpty()) return;
				ShapeSelection copiedSelection = copySelection(currentSelection);
				appState.setCopiedSelection(copiedSelection);
			}
			
		}).start();
	}
	
	private ShapeSelection copySelection(ShapeSelection srcSelection){

		CopyShapeVisitor copyVisitor = new CopyShapeVisitor(appState);
		for(BaseShape shape : srcSelection.getShapes()){
			shape.accept(copyVisitor);
		}
		Rectangle rect = srcSelection.getRect();
		Presentation presentation = appState.getPresentation();
		
		//Copy/Paste new shapes in the middle of the screen
		float posX = presentation.getHOffset() - rect.left;
		float posY = presentation.getVertOffset() - rect.top;
		posX += presentation.getScreenWidth()  / 2;
		posX -= ((rect.right - rect.left) / 2);
		posY += presentation.getScreenHeight() / 2;
		posY -= ((rect.bottom - rect.top) / 2);
		
		MoveShapeVisitor moveVisitor = 
				new MoveShapeVisitor(posX, posY);
		LinkedList<BaseShape> copiedShapes = copyVisitor.getCopy();
		for(BaseShape shape : copiedShapes){
			shape.accept(moveVisitor);
		}
		AreaSelectionVisitor selectVisitor = new AreaSelectionVisitor();
		for(BaseShape shape : copiedShapes){
			shape.accept(selectVisitor);
		}
		ShapeSelection copiedSelection = new ShapeSelection();
		copiedSelection.setShapes(copiedShapes);
		copiedSelection.setRect(selectVisitor.getSelectedArea());
		
		return copiedSelection;
	}
	
	public void pasteEvent(){
		new Thread(new Runnable(){

			@Override
			public void run() {
				ShapeSelection copiedSelection = appState.getCopiedSelection();
				if(null == copiedSelection) return;
				PasteAction pAction = new PasteAction(appState, copySelection(copiedSelection));
				pAction.execute();
				undoManager.pushUndo(pAction);
			}
			
		}).start();
	}
	
	public void toolChangedEvent(ToolChangedEvent.DrawingTool sel){
		ToolChangedEvent.DrawingTool previous = toolState.getTool();
		if(previous.equals(sel))return;
		Action changedAction = new ChangeToolAction(appState,sel, previous);
		changedAction.execute();
		undoManager.pushUndo(changedAction);
//		toolState.setTool(sel);

	}

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == FragmentActivity.RESULT_OK && null != data) {
			ImportImage importImage = new ImportImage(appState);
			importImage.startImport(data, getActivity().getContentResolver());
		}
	}

}

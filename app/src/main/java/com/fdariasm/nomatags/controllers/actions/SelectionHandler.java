package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.actions.SelectAction;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.visitors.InterceptVisitor;
import com.fdariasm.nomatags.visitors.SelectVisitor;

public class SelectionHandler extends ActionHandler{

	public SelectionHandler(AppState appState) {
		super(appState);
	}
	
	private Point startPoint;
	private Point endPoint;

	@Override
	public void actionDown(DrawEvent event) {
		startPoint = endPoint = event.getDocumentPoint();
	}

	@Override
	public void actionMove(DrawEvent event) {
		endPoint = event.getDocumentPoint();
		
		Rectangle inclusiveRect = getInclusiveRect(startPoint, endPoint);
		mCurrent.setUserSelection(inclusiveRect);
	}

	@Override
	public void actionUp(DrawEvent event) {
		endPoint = event.getDocumentPoint();
		
		Rectangle userArea = getInclusiveRect(startPoint, endPoint);
		
		SelectVisitor select = new SelectVisitor( );
		InterceptVisitor interceptVisitor = new InterceptVisitor(userArea, select);
		mDocument.accept( interceptVisitor );
		
		ShapeSelection currentSelection = new ShapeSelection();
		
		currentSelection.setShapes(select.getShapesSelected());
		currentSelection.setRect(select.getAreaSelected());

		mCurrent.setUserSelection(new Rectangle());
		
		ShapeSelection prevSelection = mDocument.getCurrentSelection();		

		mDocument.setCurrentSelection(currentSelection);
		if(!prevSelection.isEmpty() || !currentSelection.isEmpty()){
			SelectAction selectAction = new SelectAction(appState, prevSelection, currentSelection);
			addAction(selectAction);
		}
	}
	
	@Override
	public void actionCancel(DrawEvent event) {
		mCurrent.setUserSelection(new Rectangle());
	}
	
	public static Rectangle getInclusiveRect(Point startP, Point endP){
		
		float startX;
		float startY;
		float endX;
		float endY;
		if(startP.getX() < endP.getX()){
			startX = startP.getX();
			endX = endP.getX();					
		}else{
			startX = endP.getX();
			endX = startP.getX();
		}
		if(startP.getY() < endP.getY()){
			startY = startP.getY();
			endY = endP.getY();					
		}else{
			startY = endP.getY();
			endY = startP.getY();
		}
		Rectangle result = new Rectangle(startX, startY, endX, endY);
		return result;
	}

}

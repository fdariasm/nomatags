package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Presentation;

public class VertScrollHandler extends ActionHandler{
	
	private Presentation presentation;

	private float prevY;
	
	public VertScrollHandler(AppState appState) {
		super(appState);
		this.presentation = appState.getPresentation();
	}

	@Override
	public void actionDown(DrawEvent event) {
		prevY = event.getY();
		presentation.setVScrolling(true);
	}

	@Override
	public void actionMove(DrawEvent event) {
		if(presentation.canDisplaceVertically()){
			float currY = event.getY();
			presentation.changeVertOffset(currY - prevY);
			prevY = currY;
		}
	}

	@Override
	public void actionUp(DrawEvent event) {
		if(presentation.canDisplaceVertically()){
			float currY = event.getY();
			presentation.changeVertOffset(currY - prevY);
		}
		presentation.setVScrolling(false);
	}
	
	@Override
	public void actionCancel(DrawEvent event) {
	}
	

	


}

package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.actions.MoveAction;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.visitors.MoveShapeVisitor;

public class MoveShapeHandler extends ActionHandler{

	private Point initialPoint;
	private Point prevPoint;
	
	
	public MoveShapeHandler(AppState appState) {
		super(appState);
	}

	@Override
	public void actionDown(DrawEvent event) {
		initialPoint = prevPoint = event.getDocumentPoint();
	}

	@Override
	public void actionMove(DrawEvent event) {
		Point current = event.getDocumentPoint();
		float dx = current.getX() - prevPoint.getX();
		float dy = current.getY() - prevPoint.getY();
		
		moveSelection(dx, dy);
		
		prevPoint = current;
		
	}

	@Override
	public void actionUp(DrawEvent event) {
		Point current = event.getDocumentPoint();
		float dx = current.getX() - prevPoint.getX();
		float dy = current.getY() - prevPoint.getY();
		
		moveSelection(dx, dy);
		
		float totaldx = current.getX() - initialPoint.getX();
		float totaldy = current.getY() - initialPoint.getY();
		MoveAction action = new MoveAction(appState, 
				totaldx, totaldy);
		
		addAction(action);
	}
	
	@Override
	public void actionCancel(DrawEvent event) {
		
		float totaldx = prevPoint.getX() - initialPoint.getX();
		float totaldy = prevPoint.getY() - initialPoint.getY();
		moveSelection(-totaldx, -totaldy);
	}
	
	private void moveSelection(float dx, float dy){
		ShapeSelection currentSelection = mDocument.getCurrentSelection();
		
		MoveShapeVisitor visitor = new MoveShapeVisitor(dx, dy);
		for(BaseShape shape : currentSelection.getShapes()){
			shape.accept(visitor);
		}
		Rectangle rect = currentSelection.getRect();
		
//		mCurrent.areaChanged(new Point(rect.left, rect.top), 
//				new Point(rect.right, rect.bottom));
		
		rect.offset(dx, dy);
		currentSelection.getTransformPoint().movePoint(dx, dy);
		
//		mCurrent.areaChanged(new Point(rect.left, rect.top), 
//				new Point(rect.right, rect.bottom));
	}

}

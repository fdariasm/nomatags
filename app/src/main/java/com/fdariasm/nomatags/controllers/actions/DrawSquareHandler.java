package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Square;

public class DrawSquareHandler extends DrawHandler{

	private Square square;
	
	public DrawSquareHandler(AppState appState) {
		super(appState);
	}

	@Override
	public void actionDown(DrawEvent event) {
		square = shapeFactory.createSquare();
		Point startPoint = new Point(event.getX(), event.getDocumentY());
		square.setStart(startPoint);
		square.setEnd(startPoint);
		mCurrent.currentShape(square, startPoint);
		startChangedPoint = endChangedPoint = event.getPoint();
	}

	@Override
	public void actionMove(DrawEvent event) {
		Point point = new Point(event.getX(), event.getDocumentY());
		mCurrent.areaChanged(startChangedPoint, endChangedPoint);
		square.setEnd(point);
		endChangedPoint = event.getPoint();
		mCurrent.areaChanged(startChangedPoint, endChangedPoint);
	}

	@Override
	public void actionUp(DrawEvent event) {
		mCurrent.clear();
		Point currentNorm = getCurrentNorm(event, square.getStart());
		square.setEnd(currentNorm);
		
		mDocument.add(square);
		createAction(square);
	}
}

package com.fdariasm.nomatags.controllers.actions;

import android.util.Log;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Presentation;

public class HorScrollHandler extends ActionHandler{
	
	private static final String TAG = HorScrollHandler.class.getSimpleName();

	private Presentation presentation;

	private float prevH;
	
	public HorScrollHandler(AppState appState) {
		super(appState);
		this.presentation = appState.getPresentation();
	}

	@Override
	public void actionDown(DrawEvent event) {
		prevH = event.getX();
		presentation.setHScrolling(true);
	}

	@Override
	public void actionMove(DrawEvent event) {
		if(presentation.canDisplaceHorizontally()){
			Log.d(TAG, "move Can displace");
			float currX = event.getX();
			presentation.changeHOffset(currX - prevH);
			prevH = currX;
		}
	}

	@Override
	public void actionUp(DrawEvent event) {
		if(presentation.canDisplaceHorizontally()){
			Log.d(TAG, "up Can displace");
			float currX = event.getX();
			presentation.changeHOffset(currX - prevH);
		}
		presentation.setHScrolling(false);
	}
	
	@Override
	public void actionCancel(DrawEvent event) {
	}
	


}

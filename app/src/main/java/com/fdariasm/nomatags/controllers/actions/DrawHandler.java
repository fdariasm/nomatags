package com.fdariasm.nomatags.controllers.actions;

import android.util.Log;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.actions.CreateAction;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.ShapeFactory;

public abstract class DrawHandler extends ActionHandler{

	static final String TAG = DrawHandler.class.getSimpleName();
	protected ShapeFactory shapeFactory;
	protected Presentation presentation;
	
	protected Point startChangedPoint;
	protected Point endChangedPoint;
	
	public DrawHandler(AppState appState) {
		super(appState);
		
		presentation = appState.getPresentation();
		
		shapeFactory = new ShapeFactory(appState);
	}
	
	/**
	 * Helper method to create and add a createAction to UndoManager
	 * @param shape Created
	 */
	protected void createAction(BaseShape shape){
		CreateAction action =
				new CreateAction(appState, shape);
		addAction(action);
	}
	
	@Override
	public void actionCancel(DrawEvent event) {
		Log.d(TAG, "cancelling drawing");
		this.mCurrent.clear();
	}

}

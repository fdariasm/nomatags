package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Point;

public class DrawEllipseHandler extends DrawHandler{

	private Ellipse mEllipse;
	static final String TAG = DrawEllipseHandler.class.getSimpleName();


	public DrawEllipseHandler(AppState appState) {
		super(appState);
	}

	@Override
	public void actionDown(DrawEvent event) {
		mEllipse = shapeFactory.createEllipse();
		Point startPoint = event.getDocumentPoint();
		mEllipse.setStart(startPoint);
		mEllipse.setEnd(startPoint);
		startChangedPoint = endChangedPoint = event.getPoint();
		mCurrent.currentShape(mEllipse, startChangedPoint);
	}

	@Override
	public void actionMove(DrawEvent event) {
		Point p = event.getDocumentPoint();
		mCurrent.areaChanged(startChangedPoint, endChangedPoint);
		mEllipse.setEnd(p);
		endChangedPoint = event.getPoint();
		mCurrent.areaChanged(startChangedPoint, endChangedPoint);
	}

	@Override
	public void actionUp(DrawEvent event) {
		this.mCurrent.clear();
		
		Point currentPoint = getCurrentNorm(event, mEllipse.getStart());
		mEllipse.setEnd(currentPoint);
		
		mDocument.add(mEllipse);
		createAction(mEllipse);
	}
	
}

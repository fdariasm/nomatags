package com.fdariasm.nomatags.controllers.actions;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;

public class DrawLineHandler extends DrawHandler{

	private Line mLine;
	static final String TAG = DrawLineHandler.class.getSimpleName();
	
	
	public DrawLineHandler(AppState appState) {
		super(appState);
	}

	@Override
	public void actionDown(DrawEvent event) {
		mLine = shapeFactory.createLine();
		Point startPoint = event.getDocumentPoint();
		mLine.setStart(startPoint);
		mLine.setEnd(startPoint);
		startChangedPoint = endChangedPoint = event.getPoint();
		mCurrent.currentShape(mLine, startChangedPoint);
	}

	@Override
	public void actionMove(DrawEvent event) {
		Point p = event.getDocumentPoint();
		mCurrent.areaChanged(startChangedPoint, endChangedPoint);
		mLine.setEnd(p);
		endChangedPoint = event.getPoint();
		mCurrent.areaChanged(startChangedPoint, endChangedPoint);
	}

	@Override
	public void actionUp(DrawEvent event) {
		this.mCurrent.clear();
		
		Point currentPoint = getCurrentNorm(event, mLine.getStart());
		mLine.setEnd(currentPoint);
		
		mDocument.add(mLine);
		createAction(mLine);
	}
	
}

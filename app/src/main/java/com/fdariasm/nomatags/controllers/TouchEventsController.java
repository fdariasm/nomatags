package com.fdariasm.nomatags.controllers;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.fdariasm.nomatags.controllers.actions.ActionHandler;
import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.events.DrawEvent.Type;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.utils.Conversion;

public class TouchEventsController {
	
	static final String TAG = TouchEventsController.class.getSimpleName();

	private static final float THRESHOLD = 15;
	
	private int noEvents = 0;

	private Presentation mPresentation;
	
	private int initialPointerID;
		
	private ToolController 	toolController;
	
	private ScaleGestureDetector gestureDetector;

	private GestureDetectorCompat mDetector;
	
	private boolean drawEventCancelled = false;
	
	private boolean isScaling = false;
	
	public TouchEventsController(AppState appState) {
		this.mPresentation = appState.getPresentation();
		toolController = new ToolController(appState);		
	}
	
	private DrawEvent getDrawEvent(MotionEvent event){
		DrawEvent drawEvent = 
				Conversion.getEvent( event );
		drawEvent.setSx(1 / mPresentation.getScale());
		drawEvent.setSy(1 / mPresentation.getScale());
		drawEvent.setVertOffset(mPresentation.getVertOffset());
		drawEvent.sethOffset(mPresentation.getHOffset());
		
		return drawEvent;
	}

	private boolean canScroll = false;
	private int totalEvents = 0;
	
	public boolean processTouch(MotionEvent event){
		DrawEvent drawEvent = getDrawEvent(event);

		switch (event.getActionMasked()) {
		case MotionEvent.ACTION_DOWN:{
			int pointerIndex = event.getActionIndex();
			initialPointerID = event.getPointerId(pointerIndex);
			drawEventCancelled = false;
			canScroll = false;
			isScrolling = false;
			noEvents = 0;
			totalEvents = 0;
			break;
				
		}case MotionEvent.ACTION_POINTER_DOWN:{
			if( noEvents < THRESHOLD){
				canScroll = true;
				drawEvent.setType(Type.CANCEL);
				drawEventCancelled = true;
				gestureDetector.onTouchEvent(event);
				mDetector.onTouchEvent(event);
				return dispatchHandler(drawEvent);
			}
		}
		}

		totalEvents ++;
		
		gestureDetector.onTouchEvent(event);
		mDetector.onTouchEvent(event);
		
		if(drawEventCancelled)return true;
		
		switch (event.getActionMasked()) {
		case MotionEvent.ACTION_MOVE: {
			int pointerIndex = event.getActionIndex();
			int currID = event.getPointerId(pointerIndex);
			if(initialPointerID != currID)return true;
			noEvents ++;
			break;
		}
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP: {
			int pointerIndex = event.getActionIndex();
			int currID = event.getPointerId(pointerIndex);
			if(initialPointerID != currID)return true;
			noEvents = 0;
		}
		}
		
		return dispatchHandler(drawEvent);
	}
	
	private boolean dispatchHandler(DrawEvent event){
		ActionHandler mActionHandler = toolController.getHandler(event);
		switch(event.getType()){
		case DOWN:
			mActionHandler.actionDown(event);
			break;
		case MOVE:
			mActionHandler.actionMove(event);
			break;
		case UP:
			mActionHandler.actionUp(event);
			break;
		case CANCEL:
			mActionHandler.actionCancel(event);
			break;
		default:
			break;
		}
		return true;
	}

	private ScaleGestureDetector.SimpleOnScaleGestureListener scaleGestureListener =
			new ScaleGestureDetector.SimpleOnScaleGestureListener(){

		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			Log.d(TAG, "onScale x: " + detector.getFocusX() + " y: " + detector.getFocusY());
			
			if(isScaling){
				float scaleFactor = detector.getScaleFactor();
				Log.d(TAG, "Scaling: " + scaleFactor);
				
				float initialScale = mPresentation.getScale();
				
				float focusY = detector.getFocusY() / initialScale;
				float vertOffset = mPresentation.getVertOffset();
				
				float focusX = detector.getFocusX() / initialScale;
				float horizOffset = mPresentation.getHOffset();
				
				mPresentation.increaseScale(scaleFactor);
				
				float vMultiplier = mPresentation.getVMultiplier();
				float hMultiplier = mPresentation.getHMultiplier();
				
				float newVOffset = (focusY - (focusY / scaleFactor)) + vertOffset;
				newVOffset /= vMultiplier;
				
				float newHOffset = (focusX - (focusX / scaleFactor)) + horizOffset;
				newHOffset /= (hMultiplier > 0 ? hMultiplier : 1);
				
				Log.d(TAG, "new vOffset: " + newVOffset);
				Log.d(TAG, "new hOffset: " + newHOffset + " hMultiplier: " + hMultiplier);
				
				float lastScale = mPresentation.getScale();
				if(initialScale != lastScale){
					mPresentation.setVertOffset( newVOffset );
					mPresentation.setHOffset( newHOffset );
				}
				
			}
			return true;
		}
		

		@Override
		public boolean onScaleBegin(ScaleGestureDetector detector) {
			Log.d(TAG, "onScale begin: " + detector);
			Log.d(TAG, "No events: " + noEvents);
			Log.d(TAG, "IsScrolling: " + isScrolling);
			if( noEvents < THRESHOLD && !isScrolling){
				isScaling = true;
				return true;
			}
			return false;
		}

		@Override
		public void onScaleEnd(ScaleGestureDetector detector) {
			isScaling = false;
			Log.d(TAG, "onScaleEnd: " + detector);
		}
    	
    };

	private GestureDetector.SimpleOnGestureListener scrollListener = new GestureDetector.SimpleOnGestureListener(){

		@Override
		public boolean onDown(MotionEvent e) {

			return true;
		}


		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

			if(canScroll && totalEvents >= THRESHOLD && !isScaling && !isScrolling){
				isScrolling = true;
				//return true;
			}
//			isScrolling = true;
			if(isScrolling) {
				Log.d(TAG, "onScroll:  dx " + distanceX + " - dy " + distanceY);
                scrollHorizontally(distanceX);

                scrollVertically(distanceY);
			}

			return true;
		}

        private void scrollHorizontally(float distanceX) {
            if(mPresentation.canDisplaceHorizontally()){
                mPresentation.changeHOffset(distanceX);
            }
        }

        private void scrollVertically(float distanceY) {
            if(mPresentation.canDisplaceVertically())
                mPresentation.changeVertOffset(distanceY);

        }


    };

	private boolean isScrolling = false;

	public void initGestures(FragmentActivity activity) {
		this.gestureDetector = new ScaleGestureDetector(activity, scaleGestureListener);
		this.mDetector = new  GestureDetectorCompat(activity, scrollListener);

	}
}

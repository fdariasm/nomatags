package com.fdariasm.nomatags.strategies;

public interface DrawStrategyFactory {
	
	DrawShapeStrategy getDrawStrategy();

}

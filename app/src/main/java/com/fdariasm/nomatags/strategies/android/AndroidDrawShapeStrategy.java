package com.fdariasm.nomatags.strategies.android;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.util.Log;

import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;

public class AndroidDrawShapeStrategy implements DrawShapeStrategy {
	
	private Path mPath;
	private ResourceLocator systemLocator;
	

	private final static String TAG = AndroidDrawShapeStrategy.class.getCanonicalName();
	
	public AndroidDrawShapeStrategy(Path path) {
		systemLocator = ResourceLocator.getLocator();
		mPath = new Path(path);
	}

	public AndroidDrawShapeStrategy() {
		systemLocator = ResourceLocator.getLocator();
		mPath = new Path();
	}

	@Override
	public void setColor(int color) {
		systemLocator.getPaint().setColor(color);
	}

	@Override
	public void setStrokeWidth(float width) {
		systemLocator.getPaint().setStrokeWidth(width);
	}

	@Override
	public synchronized void moveTo(Point point) {
		mPath.moveTo(point.getX(), point.getY());
	}
	
	@Override
	public synchronized void lineTo(Point point) {
		mPath.lineTo(point.getX(), point.getY());
	}
	
	@Override
	public synchronized void addLine(Point a, Point b) {
		moveTo(a);
		lineTo(b);
	}

	@Override
	public synchronized  void addSquare(Point a, Point b) {
		
		mPath.addRect(a.getX(), a.getY(), b.getX(), b.getY(), Direction.CW);
	}

	@Override
	public void addEllipse(Point a, Point b) {
		RectF rect = getRectF(a, b);
		mPath.addOval(rect, Direction.CW);
	}
	
	public synchronized  RectF getRectF(Point a, Point b){
		RectF rect = new RectF();
		rect.left = a.getX();
		rect.top = a.getY();
		rect.right = b.getX();
		rect.bottom = b.getY();
		return rect;
	}

	@Override
	public synchronized void addCircle(Point point, float radius) {
		mPath.addCircle(point.getX(), point.getY(), 
				radius, Direction.CW);
	}
	
	@Override
	public synchronized void cubicTo(Point p1, Point p2, Point p3) {
		mPath.cubicTo(p1.getX(), p1.getY(), 
				p2.getX(), p2.getY(), p3.getX(), p3.getY());
	}
	
	@Override
	public synchronized  void quadTo(Point p1, Point p2) {
		mPath.quadTo(p1.getX(), p1.getY(), 
				p2.getX(), p2.getY());
	}
	
	@Override
	public void scale(float scaleFactor, Point reference) {
		Matrix scaleMatrix = new Matrix();
		scaleMatrix.setScale(scaleFactor, scaleFactor, 
				reference.getX(),
				reference.getY());
		mPath.transform(scaleMatrix);
	}
	
	 @Override
	public void rotate(float angle, Point reference) {
		 Matrix scaleMatrix = new Matrix();
		 scaleMatrix.setRotate(angle, 
				 reference.getX(),
				 reference.getY());
		 mPath.transform(scaleMatrix);
	 }

	@Override
	public void addCurve(Point[] points) {
//		mPath.c
	}

	@Override
	public synchronized void draw() {
		Paint paint = systemLocator.getPaint();
		Canvas canvas = systemLocator.getCanvas();
		canvas.drawPath(mPath, paint);
	}

	@Override
	public synchronized void offset(float dx, float dy) {
		Log.d(TAG, String.format("Offset x: %f y: %f", dx, dy));
		mPath.offset(dx, dy);
	}


	@Override
	public DrawShapeStrategy copy() {
		return new AndroidDrawShapeStrategy(mPath);
	}

}

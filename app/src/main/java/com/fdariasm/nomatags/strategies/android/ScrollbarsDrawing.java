package com.fdariasm.nomatags.strategies.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.fdariasm.nomatags.models.Presentation;

public class ScrollbarsDrawing {
	
	
	private Presentation presentation;
	
	private Paint mPaint;
	
	private static final float SCROLLBARSCALE = 0.15f;
	private static final float RADIUS = 10;

	static final String TAG = ScrollbarsDrawing.class.getSimpleName();
	
	public ScrollbarsDrawing(Presentation presentation) {
		this.presentation = presentation;
		
		mPaint = new Paint(Paint.DITHER_FLAG);
		
		mPaint.setColor(Color.BLUE);
		mPaint.setStrokeWidth(4);
		mPaint.setAlpha(180);
	}
	
	public void drawScrollbars(Canvas canvas){
		
		if(presentation.getVMultiplier() > 0.f)
			drawVerticalScrollbar(canvas);
		if(presentation.getHMultiplier() > 0.f)
			drawHorizontalScrollbar(canvas);
	}

	private void drawHorizontalScrollbar(Canvas canvas) {
		int screenHeight = presentation.getScreenHeight();
		float horOffset = presentation.getScrollHOffset();
		
		float squareHeight = presentation.isHScrolling() ?
				Presentation.HSCROLLHEIGHT : Presentation.HSCROLLHEIGHT * SCROLLBARSCALE;
				
		RectF rect = new RectF(horOffset + Presentation.MARGIN,
				screenHeight - squareHeight,
				horOffset + Presentation.HSCROLLWIDTH - Presentation.MARGIN,
				screenHeight - Presentation.MARGIN);
		canvas.drawRoundRect(
				rect, RADIUS, RADIUS, mPaint);
	}

	private void drawVerticalScrollbar(Canvas canvas) {
		int screenWidth = presentation.getScreenWidth();
		float vertOffset = presentation.getScrollVertOffset();
		
		float squareWidth = presentation.isVScrolling() ?
				Presentation.VSCROLLWIDTH : Presentation.VSCROLLHEIGHT * SCROLLBARSCALE;
				
		RectF rect = new RectF(screenWidth - squareWidth, 
				vertOffset + Presentation.MARGIN, 
				screenWidth - Presentation.MARGIN, 
				vertOffset + Presentation.VSCROLLHEIGHT - Presentation.MARGIN);
		canvas.drawRoundRect(
				rect, RADIUS, RADIUS, mPaint);
	}

}

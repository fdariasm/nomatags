package com.fdariasm.nomatags.strategies;

import com.fdariasm.nomatags.models.components.Point;

public interface DrawShapeStrategy {
	
	void setColor(int color);
	
	void setStrokeWidth(float width);
	
	void moveTo(Point point);
	
	void lineTo(Point point);
	
	void addLine(Point a, Point b);
	
	void addSquare(Point a, Point b);
	
	void addEllipse(Point a, Point b);
	
	void addCircle(Point a, float radius);
	
	void addCurve(Point[] points);
	
	void cubicTo(Point p1, Point p2, Point p3);
	
	void quadTo(Point p1, Point p2);
	
	void offset(float dx, float dy);
	
	void scale(float scaleFactor, Point reference);
	
	void rotate(float angle, Point reference);

	void draw();
	
	DrawShapeStrategy copy();

}

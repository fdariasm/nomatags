package com.fdariasm.nomatags.strategies.android;

import android.graphics.Canvas;
import android.graphics.Paint;

public class ResourceLocator {
	
	private static ResourceLocator INSTANCE;
	
	public static final int STROKE = 6;
	
	private Canvas canvasSystem;
	
	private Paint mPaint;
	
	private ResourceLocator() {

	}
	
	public static ResourceLocator getLocator(){
		if(null == INSTANCE)
			INSTANCE = new ResourceLocator();
		return INSTANCE;
	}

	public Canvas getCanvas() {
		return canvasSystem;
	}

	public void installCanvas(Canvas canvasSystem) {
		this.canvasSystem = canvasSystem;
	}

	public Paint getPaint() {
		return mPaint;
	}

	public void installPaint(Paint paintSystem) {
		this.mPaint = paintSystem;
	}

}

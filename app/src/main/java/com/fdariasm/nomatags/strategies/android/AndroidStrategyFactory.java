package com.fdariasm.nomatags.strategies.android;

import com.fdariasm.nomatags.strategies.DrawShapeStrategy;
import com.fdariasm.nomatags.strategies.DrawStrategyFactory;

public class AndroidStrategyFactory implements DrawStrategyFactory {
	
	private static AndroidStrategyFactory INSTANCE = new AndroidStrategyFactory();
	
	private AndroidStrategyFactory() {
	}
	
	public static AndroidStrategyFactory getFactory(){
		return INSTANCE;
	}
	
	@Override
	public DrawShapeStrategy getDrawStrategy() {
		
		return new AndroidDrawShapeStrategy();
	}

}

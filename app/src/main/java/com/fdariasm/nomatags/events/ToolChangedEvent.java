package com.fdariasm.nomatags.events;

public class ToolChangedEvent{
	
	public enum DrawingTool {LINE, SQUARE, CURVE, SELECT, ELLIPSE}
	
	public final DrawingTool selected;

	private DrawingTool previous;

	public ToolChangedEvent(DrawingTool tool) {
		selected = tool;
	}

	public DrawingTool getPrevious() {
		return previous;
	}

	public void setPrevious(DrawingTool previous) {
		this.previous = previous;
	}
}

package com.fdariasm.nomatags.events;

import com.fdariasm.nomatags.models.components.Point;

import java.util.List;

public class DrawEvent{
	
	public enum Type { 
		DOWN, MOVE, UP, CANCEL, OTHER
	}
	
	private float x;
	private float y;
	
	private float sx;
	private float sy;
	
	private List<Point> points;
	
	private float vertOffset;
	private float hOffset;

	
	private Type type;

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public Point getPoint(){
		return new Point(x, y);
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public float getVertOffset() {
		return vertOffset;
	}

	public void setVertOffset(float vertOffset) {
		this.vertOffset = vertOffset;
	}
	
	public float getDocumentX() {
		return x * sx;
	}

	public float getDocumentY() {
		return (y * sy)  + vertOffset;
	}

	
	public Point getDocumentPoint(){
		return new Point(x * sx, y * sy, hOffset, vertOffset);
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public float getSx() {
		return sx;
	}

	public void setSx(float sx) {
		this.sx = sx;
	}

	public float getSy() {
		return sy;
	}

	public void setSy(float sy) {
		this.sy = sy;
	}

	public float gethOffset() {
		return hOffset;
	}

	public void sethOffset(float hOffset) {
		this.hOffset = hOffset;
	}
	
}

package com.fdariasm.nomatags.events;

import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Point;

public class ShapeChangedEvent {

	private Point newPoint;
	
	private BaseShape shape;

	public BaseShape getShape() {
		return shape;
	}

	public void setShape(BaseShape shape) {
		this.shape = shape;
	}

	public Point getNewPoint() {
		return newPoint;
	}

	public void setNewPoint(Point newPoint) {
		this.newPoint = newPoint;
	}
}

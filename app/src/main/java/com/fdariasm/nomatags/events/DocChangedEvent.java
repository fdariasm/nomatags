package com.fdariasm.nomatags.events;

import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;

public class DocChangedEvent {
	
	public enum ChangeType{ADD, REMOVE, CHANGE, NEW, CHANGENEW, SELECTION, COPY, OTHER}
	
	private BaseShape shape;
	
	private ChangeType type;
	
	private int page;
	
	private Point changedPoint;
	
	private Point startChange;
	
	private Point endChange;
	
	private Rectangle selection;

	public ChangeType getType() {
		return type;
	}

	public void setType(ChangeType type) {
		this.type = type;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int index) {
		this.page = index;
	}

	public BaseShape getShape() {
		return shape;
	}

	public void setShape(BaseShape shape) {
		this.shape = shape;
	}

	public Point getChangedPoint() {
		return changedPoint;
	}

	public void setChangedPoint(Point changedPoint) {
		this.changedPoint = changedPoint;
	}

	public Point getStartChange() {
		return startChange;
	}

	public void setStartChange(Point startChange) {
		this.startChange = startChange;
	}

	public Point getEndChange() {
		return endChange;
	}

	public void setEndChange(Point endChange) {
		this.endChange = endChange;
	}

	public Rectangle getSelection() {
		return selection;
	}

	public void setSelection(Rectangle selection) {
		this.selection = selection;
	}

}

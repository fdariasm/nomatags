package com.fdariasm.nomatags.models.process;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.BaseColumns;

import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.components.Tag;
import com.fdariasm.nomatags.models.process.db_taks.DeleteTagTask;
import com.fdariasm.nomatags.models.process.db_taks.ListDocumentsByTagTask;
import com.fdariasm.nomatags.models.process.db_taks.ListDocumentsTask;
import com.fdariasm.nomatags.models.process.db_taks.ListTagsTask;
import com.fdariasm.nomatags.models.process.db_taks.LoadDocumentTask;
import com.fdariasm.nomatags.models.process.db_taks.SaveDocumentTask;
import com.fdariasm.nomatags.models.process.db_taks.SaveTagTask;

/**
 * Created by fdariasm on 31/10/2015.
 */
public class SaveDocument extends SQLiteOpenHelper {

    private static SaveDocument singleton = null;

    public static final String DATABASE_NAME = "nomatags";

    private static final int VERSION = 5;

    public static final  String TAG = SaveDocument.class.getCanonicalName();

    public enum Types{
        LINE(1, "Line"), SQUARE(2, "Square"), ELLIPSE(3, "Ellipse"), IMAGE(4, "Image"), CURVE(5, "Curve");

        public final Integer id;

        public final String name;

        Types(Integer id, String name){
            this.id = id;
            this.name = name;
        }
    }

    public static class DocumentTable implements BaseColumns {
        public static final String NAME = "document";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_CREATION_DATE = "creation_date";
        public static final String COLUMN_NAME_CURRENT_SEQ = "current_seq";
        public static final String COLUMN_NAME_PAGES = "pages";

        public static final String CREATE_TABLE = "CREATE TABLE " + NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME_NAME + " TEXT, " +
                COLUMN_NAME_CREATION_DATE + " INTEGER, " +
                COLUMN_NAME_PAGES + " INTEGER, " +
                COLUMN_NAME_CURRENT_SEQ + " INTEGER);";


        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + NAME + "";
    }

    public static class TypeTable implements BaseColumns {
        public static final String NAME = "type";
        public static final String COLUMN_NAME_NAME = "name";

        public static final String CREATE_TABLE = "CREATE TABLE " + NAME + "(" +
                _ID + " INTEGER PRIMARY KEY, " +
                COLUMN_NAME_NAME + " TEXT);";
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + NAME + "";
    }

    public static class BaseShapeTable implements BaseColumns {
        public static final String NAME = "base_shape";
        public static final String COLUMN_NAME_DOCUMENT = "document_id";
        public static final String COLUMN_NAME_DOCUMENT_SEQ = "document_seq";
        public static final String COLUMN_NAME_TYPE = "type_id";
        public static final String COLUMN_NAME_COLOR = "color";
        public static final String COLUMN_NAME_STROKE = "stroke";

        public static final String CREATE_TABLE = "CREATE TABLE " + NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME_DOCUMENT + " INTEGER, " +
                COLUMN_NAME_DOCUMENT_SEQ + " INTEGER, " +
                COLUMN_NAME_TYPE + " INTEGER, " +
                COLUMN_NAME_COLOR + " INTEGER, " +
                COLUMN_NAME_STROKE + " REAL, " +
                " FOREIGN KEY(" + COLUMN_NAME_DOCUMENT + ") REFERENCES "  + DocumentTable.NAME + "(" + DocumentTable._ID + "), " +
                " FOREIGN KEY(" + COLUMN_NAME_TYPE + ") REFERENCES "  + TypeTable.NAME + "(" + TypeTable._ID + ")" +
                ");";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + NAME + "";
    }

    public static class LSETable implements BaseColumns {
        public static final String NAME = "shape_lse";
        public static final String COLUMN_NAME_SHAPE = "shape_id";
        public static final String COLUMN_NAME_START_X = "start_x";
        public static final String COLUMN_NAME_START_Y = "start_y";
        public static final String COLUMN_NAME_END_X = "end_x";
        public static final String COLUMN_NAME_END_Y = "end_y";

        public static final String CREATE_TABLE = "CREATE TABLE " + NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME_SHAPE + " INTEGER, " +
                COLUMN_NAME_START_X + " REAL, " +
                COLUMN_NAME_START_Y + " REAL, " +
                COLUMN_NAME_END_X + " REAL, " +
                COLUMN_NAME_END_Y + " REAL, " +
                " FOREIGN KEY(" + COLUMN_NAME_SHAPE + ") REFERENCES "  + BaseShapeTable.NAME + "(" + BaseShapeTable._ID + ")" +
                ");";
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + NAME + "";
    }

    public static class ImageTable implements BaseColumns {
        public static final String NAME = "image";
        public static final String COLUMN_NAME_SHAPE = "shape_id";
        public static final String COLUMN_NAME_POS_X = "pos_x";
        public static final String COLUMN_NAME_POS_Y = "pos_y";
        public static final String COLUMN_NAME_IMAGE_URI= "image_uri";

        public static final String CREATE_TABLE = "CREATE TABLE " + NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME_SHAPE + " INTEGER, " +
                COLUMN_NAME_POS_X + " REAL, " +
                COLUMN_NAME_POS_Y + " REAL, " +
                COLUMN_NAME_IMAGE_URI + " TEXT, " +
                " FOREIGN KEY(" + COLUMN_NAME_SHAPE + ") REFERENCES "  + BaseShapeTable.NAME + "(" + BaseShapeTable._ID + ")" +
                ");";
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + NAME + "";
    }

    public static class CurveTable implements BaseColumns {
        public static final String NAME = "curve";
        public static final String COLUMN_NAME_SHAPE = "shape_id";
        public static final String COLUMN_NAME_POS_X = "pos_x";
        public static final String COLUMN_NAME_POS_Y = "pos_y";

        public static final String CREATE_TABLE = "CREATE TABLE " + NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME_SHAPE + " INTEGER, " +
                COLUMN_NAME_POS_X + " REAL, " +
                COLUMN_NAME_POS_Y + " REAL, " +
                " FOREIGN KEY(" + COLUMN_NAME_SHAPE + ") REFERENCES "  + BaseShapeTable.NAME + "(" + BaseShapeTable._ID + ")" +
                ");";
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + NAME + "";
    }

    public static class TagsTable implements BaseColumns {
        public static final String NAME = "tag";
        public static final String COLUMN_NAME_NAME = "name";

        public static final String CREATE_TABLE = "CREATE TABLE " + NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME_NAME + " TEXT);";
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + NAME + "";
    }

    public static class DocumentTagsTable implements BaseColumns {
        public static final String NAME = "document_tags";
        public static final String COLUMN_NAME_TAGID = "TAG_ID";
        public static final String COLUMN_NAME_DOCID = "document_id";

        public static final String CREATE_TABLE = "CREATE TABLE " + NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME_TAGID + " INTEGER, " +
                COLUMN_NAME_DOCID + " INTEGER, " +
                " FOREIGN KEY(" + COLUMN_NAME_TAGID + ") REFERENCES "  + TagsTable.NAME + "(" + TagsTable._ID + ")," +
                " FOREIGN KEY(" + COLUMN_NAME_DOCID + ") REFERENCES "  + DocumentTable.NAME + "(" + DocumentTable._ID + ")" +
                ");";
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + NAME + "";
    }

    public synchronized static SaveDocument getInstance(Context ctxt) {
        if (singleton == null) {
            singleton = new SaveDocument(ctxt.getApplicationContext());
        }
        return(singleton);
    }

    private SaveDocument(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try{
            db.beginTransaction();

            doCreateTables(db);

            db.setTransactionSuccessful();

        }finally{
            db.endTransaction();
        }

    }

    private void doCreateTables(SQLiteDatabase db){
        db.execSQL(DocumentTable.CREATE_TABLE);
        db.execSQL(TypeTable.CREATE_TABLE);
        insertDefaultTypes(db);
        db.execSQL(BaseShapeTable.CREATE_TABLE);
        db.execSQL(LSETable.CREATE_TABLE);
        db.execSQL(ImageTable.CREATE_TABLE);
        db.execSQL(CurveTable.CREATE_TABLE);
        db.execSQL(TagsTable.CREATE_TABLE);
        db.execSQL(DocumentTagsTable.CREATE_TABLE);
    }

    private void insertDefaultTypes(SQLiteDatabase db) {
        for(Types t :Types.values()){
            ContentValues values = new ContentValues();
            values.put(TypeTable._ID, t.id);
            values.put(TypeTable.COLUMN_NAME_NAME, t.name);

            db.insert(TypeTable.NAME, null, values);
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try{
            db.beginTransaction();

            clearDatabase(db);
            doCreateTables(db);

            db.setTransactionSuccessful();

        }finally{
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try{
            db.beginTransaction();

            clearDatabase(db);
            doCreateTables(db);

            db.setTransactionSuccessful();

        }finally{
            db.endTransaction();
        }

    }

    private void clearDatabase(SQLiteDatabase db) {
        db.execSQL(LSETable.DROP_TABLE);
        db.execSQL(ImageTable.DROP_TABLE);
        db.execSQL(CurveTable.DROP_TABLE);

        db.execSQL(DocumentTagsTable.DROP_TABLE);
        db.execSQL(TagsTable.DROP_TABLE);

        db.execSQL(DocumentTable.DROP_TABLE);
        db.execSQL(TypeTable.DROP_TABLE);
        db.execSQL(BaseShapeTable.DROP_TABLE);
    }

    public void saveDocumentState(DocumentShapes document){
        executeAsyncTask(new SaveDocumentTask(this), document);
    }

    public void loadDocument(Integer documentId, OnDocumentLoadedListener listener){
        executeAsyncTask(new LoadDocumentTask(listener, this), documentId);
    }

    public void listDocuments(OnCursorResultsListener listener){
        executeAsyncTask(new ListDocumentsTask(listener, this) ,
                new ListDocumentsTask.QueryDoc(null, new String[]{}));
    }

    public void listDocumentsByName(OnCursorResultsListener listener, String name){
        executeAsyncTask(new ListDocumentsTask(listener, this),
                new ListDocumentsTask.QueryDoc(
                        DocumentTable.COLUMN_NAME_NAME + " like ?",
                        new String[]{"%" + name + "%"}
                ));
    }

    public void listDocumentsByTag(OnCursorResultsListener listener, Integer tag){
        executeAsyncTask(new ListDocumentsByTagTask(listener, this), tag);
    }

    public void loadTag(String name, OnTagsListener listener){
        executeAsyncTask(new SaveTagTask(listener, this), name);
    }

    public void listTags(String name, OnCursorResultsListener listener){
        executeAsyncTask(new ListTagsTask(listener, this), name);
    }

    public void deleteTag(Integer tagId, OnTagsListener listener){
        executeAsyncTask(new DeleteTagTask(listener, this), tagId);
    }

    @TargetApi(11)
    public static <T> void executeAsyncTask(AsyncTask<T, ?, ?> task,
                                            T... params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        }
        else {
            task.execute(params);
        }
    }

    public interface OnDocumentLoadedListener{
        void updateState(DocumentShapes document);
    }

    public interface OnCursorResultsListener {
        void showResults(Cursor results);
    }


    public interface OnTagsListener{
        void tagAdded(Tag tag);

        void tagDeleted();
    }
}

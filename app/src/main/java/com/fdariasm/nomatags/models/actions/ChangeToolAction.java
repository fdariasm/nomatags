package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.events.ToolChangedEvent;

public class ChangeToolAction extends Action{

    private ToolChangedEvent.DrawingTool currentTool;
    private ToolChangedEvent.DrawingTool previousTool;
    private final ShapeSelection prevSelection;

    public ChangeToolAction(AppState appState, ToolChangedEvent.DrawingTool currentTool,
                            ToolChangedEvent.DrawingTool previousTool) {
        super(appState);
        this.currentTool = currentTool;
        this.previousTool = previousTool;


        prevSelection = mDocument.getCurrentSelection();
    }

    @Override
    public void execute() {
        toolState.setTool(currentTool);
        if(prevSelection.isEmpty())return;
        ShapeSelection currentSelection = new ShapeSelection();
        mDocument.setCurrentSelection(currentSelection);
    }

    @Override
    public void unexecute() {
        toolState.setTool(previousTool);
        if(prevSelection.isEmpty())return;
        mDocument.setCurrentSelection(prevSelection);
    }

//    private void removeSelection() {
//        ShapeSelection prevSelection = document.getCurrentSelection();
//        if(prevSelection.isEmpty())return;
//        ShapeSelection currentSelection = new ShapeSelection();
//        document.setCurrentSelection(currentSelection);
//
//        SelectAction selectAction = new SelectAction(appState, prevSelection, currentSelection);
//        undoManager.pushUndo(selectAction);
//
//    }
}

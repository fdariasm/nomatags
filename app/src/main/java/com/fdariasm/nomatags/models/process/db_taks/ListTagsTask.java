package com.fdariasm.nomatags.models.process.db_taks;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.fdariasm.nomatags.models.process.SaveDocument;
import com.fdariasm.nomatags.models.process.SaveDocument.OnCursorResultsListener;

/**
 * Created by fdariasm on 03/11/2015.
 */
public class ListTagsTask extends AsyncTask<String, Void, Cursor> {

    private OnCursorResultsListener listener;

    private SaveDocument saveDocument;

    public ListTagsTask(OnCursorResultsListener listener,
                        SaveDocument saveDocument){
        this.listener = listener;
        this.saveDocument = saveDocument;
    }

    @Override
    protected Cursor doInBackground(String... params) {

        String q = params[0];

        SQLiteDatabase rdb = saveDocument.getReadableDatabase();
        Cursor cursor = rdb.query(SaveDocument.TagsTable.NAME, null,
                SaveDocument.TagsTable.COLUMN_NAME_NAME + " like ?",
                new String[]{"%" + q + "%"}, null, null,
                SaveDocument.TagsTable.COLUMN_NAME_NAME + " ASC");
        cursor.getCount();
        return cursor;
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        if(null != cursor)
            listener.showResults(cursor);
    }

}

package com.fdariasm.nomatags.models.process;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fdariasm.nomatags.R;
import com.fdariasm.nomatags.models.components.Tag;

import java.util.ArrayList;

/**
 * Created by fdariasm on 04/11/2015.
 */
public class TagsAdapter extends ArrayAdapter<Tag> {

    public TagsAdapter(Context context, ArrayList<Tag> tags) {
        super(context, 0, tags);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Tag tag = getItem(position);
        android.util.Log.e("GetView", "tag:" + tag.getName());
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.tag_row, parent, false);
        }
        // Lookup view for data population
        TextView tagName = (TextView) convertView.findViewById(R.id.tag_name);
        tagName.setTag(position);
        tagName.setText(tag.getName());

        return convertView;
    }
}

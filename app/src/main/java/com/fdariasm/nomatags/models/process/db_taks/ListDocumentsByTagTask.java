package com.fdariasm.nomatags.models.process.db_taks;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.fdariasm.nomatags.models.process.SaveDocument;
import com.fdariasm.nomatags.models.process.SaveDocument.DocumentTable;
import com.fdariasm.nomatags.models.process.SaveDocument.DocumentTagsTable;
import com.fdariasm.nomatags.models.process.SaveDocument.OnCursorResultsListener;

/**
 * Created by fdariasm on 03/11/2015.
 */
public class ListDocumentsByTagTask extends AsyncTask<Integer, Void, Cursor> {

    private OnCursorResultsListener listener;

    private SaveDocument saveDocument;

    public ListDocumentsByTagTask(OnCursorResultsListener listener,
                                  SaveDocument saveDocument){
        this.listener = listener;
        this.saveDocument = saveDocument;
    }

    @Override
    protected Cursor doInBackground(Integer... params) {

        Integer idTag = params[0];

        SQLiteDatabase rdb = saveDocument.getReadableDatabase();
        Cursor cursor = rdb.rawQuery(
                "select " + DocumentTable.NAME + ".* from " + DocumentTable.NAME +
                        " join " + DocumentTagsTable.NAME + " on " +
                        DocumentTable.NAME + "." + DocumentTable._ID + " = " +
                        DocumentTagsTable.NAME + "." + DocumentTagsTable.COLUMN_NAME_DOCID +
                        " where " + DocumentTagsTable.NAME + "." + DocumentTagsTable.COLUMN_NAME_TAGID + " = ?;",
                new String[]{idTag.toString()});

        cursor.getCount();
        return cursor;
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        if(null != cursor)
            listener.showResults(cursor);
    }
}

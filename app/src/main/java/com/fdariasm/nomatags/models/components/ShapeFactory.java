package com.fdariasm.nomatags.models.components;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.ToolState;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;

public class ShapeFactory {
	
	private DocumentShapes document;
	private ToolState toolState;
	
	public ShapeFactory(AppState appState) {
		this.document = appState.getDocument();
		toolState = appState.getToolState();
	}
	
	public Curve createCurve(DrawShapeStrategy strategy){
		long nextSeq = document.getNextSeq();
		Curve curve = new Curve(nextSeq, strategy);
		curve.setColor(toolState.getPaintColor());
		curve.setStrokeWidth(toolState.getPaintStrokeWidth());
		return curve;
	}
	
	public Line createLine(){
		Line line = new Line(document.getNextSeq());
		line.setStrokeWidth(toolState.getPaintStrokeWidth());
		line.setColor(toolState.getPaintColor());
		return line;
	}

	public Ellipse createEllipse(){
        Ellipse ellipse = new Ellipse(document.getNextSeq());
		ellipse.setStrokeWidth(toolState.getPaintStrokeWidth());
		ellipse.setColor(toolState.getPaintColor());
		return ellipse;
	}


	public Square createSquare(){
		Square square = new Square(document.getNextSeq());
		square.setStrokeWidth(toolState.getPaintStrokeWidth());
		square.setColor(toolState.getPaintColor());
		return square;
	}

}

package com.fdariasm.nomatags.models.components;

import com.fdariasm.nomatags.events.ShapeChangedEvent;
import com.fdariasm.nomatags.listeners.ShapeChangedListener;
import com.fdariasm.nomatags.visitors.ShapeVisitor;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public abstract class BaseShape implements Serializable{
	
	protected int color;
	protected float strokeWidth;
	
	private Set<ShapeChangedListener> mListeners =
			new HashSet<ShapeChangedListener>();
	
	protected long seqNumber;
	
	protected Rectangle shapeRect = new Rectangle();
	
	public BaseShape(long seq) {
		this.seqNumber = seq;
	}

	public abstract void accept(ShapeVisitor visitor);

	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public float getStrokeWidth() {
		return strokeWidth;
	}
	public void setStrokeWidth(float strokeWidth) {
		this.strokeWidth = strokeWidth;
	}
	
	public void addChangedListener(ShapeChangedListener listener){
		mListeners.add(listener);
	}
	
	public void removeChangedListener(ShapeChangedListener listener){
		mListeners.remove(listener);
	}
	
	public Set<ShapeChangedListener> getListeners(){
		return mListeners;
	}
	
	public void addListeners(Set<ShapeChangedListener> listeners){
		mListeners.addAll(listeners);
	}
	
	public void removeListeners(Set<ShapeChangedListener> listeners){
		mListeners.removeAll(listeners);
	}
	
	protected void notifyChange(Point newPoint){
		ShapeChangedEvent event = new ShapeChangedEvent();
		event.setShape(this);
		event.setNewPoint(newPoint);
		for(ShapeChangedListener listener : mListeners){
			listener.shapeChanged(event);
		}
	}
	
	public void notifyListeners(){
		ShapeChangedEvent event = new ShapeChangedEvent();
		event.setShape(this);
		for(ShapeChangedListener listener : mListeners){
			listener.shapeChanged(event);
		}
	}
	
	public long getSeqNumber() {
		return seqNumber;
	}
		
	synchronized public Rectangle getRect(){
		return shapeRect;
	}
	
	synchronized public void expandArea(Point point){
		float x = point.getX();
		float y = point.getY();
		if(x < shapeRect.left)
			shapeRect.left = x;
		if(x > shapeRect.right)
			shapeRect.right = x;
		if(y < shapeRect.top)
			shapeRect.top = y;
		if(y > shapeRect.bottom)
			shapeRect.bottom = y;
	}
	
	synchronized public void initArea(Point point){
		shapeRect.left = shapeRect.right = point.getX();
		shapeRect.top = shapeRect.bottom = point.getY();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (seqNumber ^ (seqNumber >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseShape other = (BaseShape) obj;
		if (seqNumber != other.seqNumber)
			return false;
		return true;
	}

}

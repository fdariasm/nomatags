package com.fdariasm.nomatags.models.process.db_taks;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.fdariasm.nomatags.models.process.SaveDocument;

/**
 * Created by fdariasm on 04/11/2015.
 */
public class DeleteTagTask extends AsyncTask<Integer, Void, Void> {

    private SaveDocument.OnTagsListener listener;
    private SaveDocument saveDocument;

    private static final  String TAG = DeleteTagTask.class.getCanonicalName();

    public DeleteTagTask(SaveDocument.OnTagsListener listener,
                         SaveDocument saveDocument){
        this.listener = listener;
        this.saveDocument = saveDocument;
    }

    @Override
    protected Void doInBackground(Integer... params) {
        Integer idTag = params[0];
        SQLiteDatabase wdb = saveDocument.getWritableDatabase();
        try{
            wdb.beginTransaction();

            wdb.delete(SaveDocument.DocumentTagsTable.NAME,
                    SaveDocument.DocumentTagsTable.COLUMN_NAME_TAGID + " = ?",
                    new String[]{idTag.toString()});

            wdb.delete(SaveDocument.TagsTable.NAME,
                    SaveDocument.TagsTable._ID + " = ?",
                    new String[]{idTag.toString()});

            wdb.setTransactionSuccessful();
        }finally {
            wdb.endTransaction();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        listener.tagDeleted();
    }
}

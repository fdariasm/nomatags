package com.fdariasm.nomatags.models;

import com.fdariasm.nomatags.events.DocChangedEvent;
import com.fdariasm.nomatags.events.DocChangedEvent.ChangeType;
import com.fdariasm.nomatags.events.ShapeChangedEvent;
import com.fdariasm.nomatags.listeners.DocumentListener;
import com.fdariasm.nomatags.listeners.ShapeChangedListener;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Tag;
import com.fdariasm.nomatags.visitors.ShapeVisitor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DocumentShapes implements Serializable{

	private Long id;
	private long currentSeq = 0;
	private LinkedList<BaseShape> content;

	private Integer pages;

    private String name;

	private ShapeSelection currentSelection = new ShapeSelection();
	
	private transient Object lock = new Object();

	private DocumentListeners mDocListeners = new DocumentListeners();

	private Date creationDate;

    private Set<Tag> tags = new HashSet<Tag>();
						
	public DocumentShapes() {		
		content = new LinkedList<BaseShape>();
	}

    private void readObject(ObjectInputStream in) throws IOException,ClassNotFoundException {
        in.defaultReadObject();
        lock = new Object();
        normalShapeListener = new ShapeChangedListenerImp();
    }

    public void addListener(DocumentListener listener){
		mDocListeners.addListener(listener);
	}
	
	public void removeListener(DocumentListener listener){
		mDocListeners.removeListener(listener);
	}
	
	public void add(BaseShape shape){
		synchronized(lock){
			content.add(shape);
		}
		shape.addChangedListener(normalShapeListener);
			
		DocChangedEvent event = new DocChangedEvent();
		event.setShape(shape);
		event.setType(ChangeType.ADD);
		mDocListeners.notifyListeners(event);
	}
	
	public void addAll(List<BaseShape> shapes){
		synchronized (lock) {
			content.addAll(shapes);
		}
        for(BaseShape bs : shapes){
            bs.addChangedListener(normalShapeListener);
        }

		DocChangedEvent event = new DocChangedEvent();
		event.setType(ChangeType.COPY);
		mDocListeners.notifyListeners(event);
	}

    public Set<Tag> getTags(){
        return tags;
    }

    public void addTag(Tag tag){
        tags.add(tag);
    }

    public void removeTag(Tag tag){
        tags.remove(tag);
    }

    public void setTags(Set<Tag> tags){
        this.tags = tags;
    }

	public void removeAll(List<BaseShape> shapes){
		synchronized (lock) {
			content.removeAll(shapes);
		}
		DocChangedEvent event = new DocChangedEvent();
		event.setType(ChangeType.COPY);
		mDocListeners.notifyListeners(event);
	}
	
	
	public void remove(BaseShape shape){
		synchronized(lock){
			content.remove(shape);
		}
		shape.removeChangedListener(normalShapeListener);
		
		DocChangedEvent event = new DocChangedEvent();
		event.setType(ChangeType.REMOVE);
		event.setShape(shape);
		mDocListeners.notifyListeners(event);
	}
	
	public long getNextSeq(){
		return currentSeq++;
	}

    public long getCurrentSeq(){
        return currentSeq;
    }

    public void setCurrentSeq(long currSeq){
        this.currentSeq = currSeq;
    }
	
	public void accept(ShapeVisitor visitor){
		synchronized(lock){
			for(BaseShape shape : content){
				shape.accept(visitor);
			}
		}
	}
	
	public ShapeSelection getCurrentSelection() {
		return currentSelection;
	}

	public void setCurrentSelection(ShapeSelection currentSelection) {
		this.currentSelection = currentSelection;
		DocChangedEvent change = new DocChangedEvent();
		change.setType(ChangeType.SELECTION);
		change.setSelection(currentSelection.getRect());
		mDocListeners.notifyListeners(change);
	}

	private transient ShapeChangedListener normalShapeListener = new ShapeChangedListenerImp();

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

	public Integer getPages() {
		return pages;
	}

	public void setPages(Integer pages) {
		this.pages = pages;
	}

	private class ShapeChangedListenerImp implements ShapeChangedListener{
        @Override
        public void shapeChanged(ShapeChangedEvent event) {
            DocChangedEvent change = new DocChangedEvent();
            change.setType(ChangeType.CHANGE);
            change.setShape(event.getShape());
            mDocListeners.notifyListeners(change);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}

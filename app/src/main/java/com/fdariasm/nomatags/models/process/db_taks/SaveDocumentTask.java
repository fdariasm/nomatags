package com.fdariasm.nomatags.models.process.db_taks;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.components.Tag;
import com.fdariasm.nomatags.models.process.SaveDocument;
import com.fdariasm.nomatags.models.process.SaveDocument.DocumentTable;
import com.fdariasm.nomatags.utils.Conversion;
import com.fdariasm.nomatags.visitors.PersistVisitor;

import java.io.File;
import java.util.Date;
import java.util.Set;

/**
 * Created by fdariasm on 03/11/2015.
 */
public class SaveDocumentTask extends AsyncTask<DocumentShapes, Void, DocumentShapes> {

    private static final String TAG = SaveDocumentTask.class.getCanonicalName();
    private SaveDocument saveDocument;
    public SaveDocumentTask(SaveDocument saveDocument){
        this.saveDocument= saveDocument;
    }

    @Override
    protected DocumentShapes doInBackground(DocumentShapes... params) {
        DocumentShapes document = params[0];
        SQLiteDatabase wdb = saveDocument.getWritableDatabase();
        try{
            wdb.beginTransaction();

            if( null == document.getId() ){
                insertDocument(document, wdb);
            }else
                updateDocument(document, wdb);


            wdb.setTransactionSuccessful();
        }finally {
            wdb.endTransaction();
        }
        return document;
    }

    private void updateDocument(DocumentShapes document, SQLiteDatabase wdb) {

        ContentValues values = new ContentValues();
        values.put(DocumentTable.COLUMN_NAME_NAME,  document.getName());
        values.put(DocumentTable.COLUMN_NAME_PAGES, document.getPages());
        values.put(DocumentTable.COLUMN_NAME_CURRENT_SEQ, document.getCurrentSeq());

        wdb.update(DocumentTable.NAME, values, DocumentTable._ID + " = ?",
                new String[]{document.getId().toString()});

        updateShapes(document, wdb);
        updateTags(document, wdb);
    }

    private void updateTags(DocumentShapes document, SQLiteDatabase wdb) {
        clearTags(document, wdb);
        insertTags(document, document.getTags(), wdb);
    }

    private void clearTags(DocumentShapes document, SQLiteDatabase wdb) {
        wdb.delete(SaveDocument.DocumentTagsTable.NAME,
                SaveDocument.DocumentTagsTable.COLUMN_NAME_DOCID + " = ?",
                new String[]{document.getId().toString()});

    }

    private void updateShapes(DocumentShapes document, SQLiteDatabase wdb) {
        cleanShapes(document, wdb);
        insertShapes(document, wdb);
    }

    private void cleanShapes(DocumentShapes document, SQLiteDatabase wdb) {
        SQLiteDatabase rdb = saveDocument.getReadableDatabase();

        Cursor cursor = rdb.query(SaveDocument.BaseShapeTable.NAME,
                null,
                SaveDocument.BaseShapeTable.COLUMN_NAME_DOCUMENT + "=?",
                new String[]{document.getId().toString()},
                null, null, null
        );

        while (cursor.moveToNext()){
            Integer type = cursor.getInt(cursor.getColumnIndex(
                    SaveDocument.BaseShapeTable.COLUMN_NAME_TYPE));
            Integer shapeId = cursor.getInt(cursor.getColumnIndex(
                    SaveDocument.BaseShapeTable._ID));
            if(type.equals(SaveDocument.Types.LINE.id)
                || type.equals(SaveDocument.Types.SQUARE.id)
                    || type.equals(SaveDocument.Types.ELLIPSE.id)){
                wdb.delete(SaveDocument.LSETable.NAME,
                        SaveDocument.LSETable.COLUMN_NAME_SHAPE + " = ?",
                        new String[]{shapeId.toString()});
            }else if(type.equals(SaveDocument.Types.CURVE.id)){
                wdb.delete(SaveDocument.CurveTable.NAME,
                        SaveDocument.CurveTable.COLUMN_NAME_SHAPE + " = ?",
                        new String[]{shapeId.toString()});
            }else if(type.equals(SaveDocument.Types.IMAGE.id)) {
                clearImage(shapeId, rdb, wdb);
            }
            wdb.delete(SaveDocument.BaseShapeTable.NAME, SaveDocument.BaseShapeTable._ID + "= ?",
                    new String[]{shapeId.toString()});
        }
        cursor.close();
    }

    private void clearImage(Integer shapeId, SQLiteDatabase rdb, SQLiteDatabase wdb) {
        Cursor cursor = rdb.query(SaveDocument.ImageTable.NAME, null, SaveDocument.ImageTable.COLUMN_NAME_SHAPE + " = ?",
                new String[]{shapeId.toString()}, null, null, null);

        cursor.moveToFirst();
        if(cursor.isAfterLast()){
            return;
        }
        String filePath =
                cursor.getString(cursor.getColumnIndex(SaveDocument.ImageTable.COLUMN_NAME_IMAGE_URI));
        File file = new File(filePath);
        if(file.exists())
            file.delete();

        wdb.delete(SaveDocument.ImageTable.NAME,
                SaveDocument.ImageTable.COLUMN_NAME_SHAPE + " = ?",
                new String[]{shapeId.toString()});
        cursor.close();
    }

    private void insertShapes(DocumentShapes document, SQLiteDatabase wdb) {
        PersistVisitor visitor = new PersistVisitor(wdb,document);

        document.accept(visitor);
    }

    private void insertDocument(DocumentShapes document, SQLiteDatabase wdb) {
        ContentValues values = new ContentValues();

        values.put(DocumentTable.COLUMN_NAME_NAME,  document.getName());
        values.put(DocumentTable.COLUMN_NAME_CREATION_DATE, Conversion.persistDate(new Date()));
        values.put(DocumentTable.COLUMN_NAME_CURRENT_SEQ, document.getCurrentSeq());
        values.put(DocumentTable.COLUMN_NAME_PAGES, document.getPages());

        long insertedId = wdb.insert(DocumentTable.NAME, null, values);
        document.setId(insertedId);

        insertShapes(document, wdb);
        insertTags(document, document.getTags(), wdb);
    }

    private void insertTags(DocumentShapes document, Set<Tag> tags, SQLiteDatabase wdb) {
        for(Tag tag : tags){
            ContentValues values = new ContentValues();

            values.put(SaveDocument.DocumentTagsTable.COLUMN_NAME_DOCID,  document.getId());
            values.put(SaveDocument.DocumentTagsTable.COLUMN_NAME_TAGID,  tag.getId());

            wdb.insert(SaveDocument.DocumentTagsTable.NAME, null, values);
        }
    }

    @Override
    protected void onPostExecute(DocumentShapes document) {
        android.util.Log.e(TAG, "Completed: save_doc document " + document.getId());
    }
}

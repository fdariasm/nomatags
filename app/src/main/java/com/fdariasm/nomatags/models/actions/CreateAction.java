package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.components.BaseShape;

public class CreateAction extends Action{

	private BaseShape mShape;
		
	public CreateAction(AppState appState, BaseShape shape) {
		super(appState);
		mShape = shape;
	}

	@Override
	public void execute() {
		mDocument.add(mShape);
		toolState.setTool(prevTool);
	}

	@Override
	public void unexecute() {
		mDocument.remove(mShape);
	}

}

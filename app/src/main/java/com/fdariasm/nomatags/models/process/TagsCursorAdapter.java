package com.fdariasm.nomatags.models.process;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.fdariasm.nomatags.R;

/**
 * Created by fdariasm on 03/11/2015.
 */
@SuppressWarnings("deprecation")
public class TagsCursorAdapter extends SimpleCursorAdapter {

    public TagsCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
    }

    public TagsCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndex(SaveDocument.TagsTable._ID));
        String name = cursor.getString(cursor.getColumnIndex(SaveDocument.TagsTable.COLUMN_NAME_NAME));

        view.setTag(id);

        super.bindView(view, context, cursor);

        ((TextView) view.findViewById(R.id.tag_name)).setText(name);
    }
}

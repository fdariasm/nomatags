package com.fdariasm.nomatags.models.process;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.fdariasm.nomatags.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by fdariasm on 03/11/2015.
 */
@SuppressWarnings("deprecation")
public class DocumentsCursorAdapter extends SimpleCursorAdapter {


    private String format = "MM/dd/yyyy h:mm a";
    private SimpleDateFormat sdf = new SimpleDateFormat(format);

    public DocumentsCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
    }

    public DocumentsCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndex(SaveDocument.DocumentTable._ID));
        long time = cursor.getLong(cursor.getColumnIndex(SaveDocument.DocumentTable.COLUMN_NAME_CREATION_DATE));
        view.setTag(id);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);

        String dateString = sdf.format(cal.getTime());

        super.bindView(view, context, cursor);

        ((TextView) view.findViewById(R.id.doc_date)).setText(dateString);
    }
}

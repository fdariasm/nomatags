package com.fdariasm.nomatags.models;

import java.util.HashSet;
import java.util.Set;

import com.fdariasm.nomatags.events.ToolChangedEvent;
import com.fdariasm.nomatags.listeners.ToolChangedListener;

public class ToolState {
	
	private Set<ToolChangedListener> listeners = new HashSet<ToolChangedListener>();
	
	private ToolChangedEvent.DrawingTool tool;
	
	private float paintStrokeWidth = 6;
	private int paintColor = 0xFF000000;
	
	public void addListener(ToolChangedListener listener){
		listeners.add(listener);
	}
	
	public void removeListener(ToolChangedListener listener){
		listeners.remove(listener);
	}
	
	protected void notifyListeners(ToolChangedEvent event){
		for(ToolChangedListener listener : listeners){
			listener.toolChanged(event);
		}
	}

	public ToolChangedEvent.DrawingTool getTool() {
		return tool;
	}

	public void setTool(ToolChangedEvent.DrawingTool tool) {
		ToolChangedEvent.DrawingTool previous = this.tool;
		this.tool = tool;
		ToolChangedEvent event = new ToolChangedEvent(tool);
		event.setPrevious(previous);
		notifyListeners(event);
	}

	public int getPaintColor() {
		return paintColor;
	}

	public void setPaintColor(int paintColor) {
		this.paintColor = paintColor;
	}

	public float getPaintStrokeWidth() {
		return paintStrokeWidth;
	}

	public void setPaintStrokeWidth(float paintStrokeWidth) {
		this.paintStrokeWidth = paintStrokeWidth;
	}

}

package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.ShapeSelection;

public class PasteAction extends Action{

	private ShapeSelection shapeSelection;
	private ShapeSelection prevSelection;

	public PasteAction(AppState appState, ShapeSelection shapes) {
		super(appState);
		this.shapeSelection = shapes;
	}

	@Override
	public void execute() {
		mDocument.addAll(shapeSelection.getShapes());
		prevSelection = mDocument.getCurrentSelection();
		mDocument.setCurrentSelection(shapeSelection);
	}

	@Override
	public void unexecute() {
		mDocument.removeAll(shapeSelection.getShapes());
		if( null != prevSelection)
			mDocument.setCurrentSelection(prevSelection);
	}
	

}

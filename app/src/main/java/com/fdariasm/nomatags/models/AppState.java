package com.fdariasm.nomatags.models;

import com.fdariasm.nomatags.listeners.DocumentListener;
import com.fdariasm.nomatags.listeners.PresentationListener;
import com.fdariasm.nomatags.listeners.ToolChangedListener;
import com.fdariasm.nomatags.models.actions.UndoManager;

public class AppState {
	
	private DocumentShapes document;

	private Current current;

	private Presentation presentation;
	
	private ToolState toolState;

	private UndoManager undoManager;
	
	private ShapeSelection copiedSelection;
	
	private final Object lock = new Object();
	
	public AppState() {
		document = new DocumentShapes();
		presentation = new Presentation();
		current = new Current();
		toolState = new ToolState();

		undoManager = UndoManager.getManager();
	}

	public AppState(DocumentShapes document) {
		this.document = document;
		presentation = new Presentation();
		presentation.setPages(document.getPages());
		current = new Current();
		toolState = new ToolState();

		undoManager = UndoManager.getManager();
	}
	
	public DocumentShapes getDocument(){
		return document;
	}
	
	public Current getCurrent(){
		return current;
	}
	
	public Presentation getPresentation(){
		return presentation;
	}
	
	public void addNewPage(){
		presentation.addNewPage();
	}
	
	public void addDocumentListener(DocumentListener listener){
		document.addListener(listener);
		current.addListener(listener);
	}
	
	public void removeDocumentListener(DocumentListener listener){
		document.removeListener(listener);
		current.addListener(listener);
	}
	
	public void addToolListener(ToolChangedListener listener){
		toolState.addListener(listener);
	}
	
	public void removeToolListener(ToolChangedListener listener){
		toolState.removeListener(listener);
	}

	public ToolState getToolState() {
		return toolState;
	}

	public UndoManager getUndoManager() {
		return undoManager;
	}

	public ShapeSelection getCopiedSelection() {
		synchronized (lock) {
			return copiedSelection;
		}
	}

	public void setCopiedSelection(ShapeSelection copiedSelection) {
		synchronized (lock) {
			this.copiedSelection = copiedSelection;
		}
	}

    public void addPresentationListener(PresentationListener presentationListener) {
        presentation.addListener(presentationListener);
    }
}

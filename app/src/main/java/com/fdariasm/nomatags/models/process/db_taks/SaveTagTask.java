package com.fdariasm.nomatags.models.process.db_taks;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.fdariasm.nomatags.models.components.Tag;
import com.fdariasm.nomatags.models.process.SaveDocument;

/**
 * Created by fdariasm on 04/11/2015.
 */
public class SaveTagTask extends AsyncTask<String, Void, Tag> {

    private SaveDocument.OnTagsListener listener;
    private SaveDocument saveDocument;

    private static final  String TAG = SaveTagTask.class.getCanonicalName();

    public SaveTagTask(SaveDocument.OnTagsListener listener,
                       SaveDocument saveDocument){
        this.listener = listener;
        this.saveDocument = saveDocument;
    }

    @Override
    protected Tag doInBackground(String... params) {
        String name = params[0];
        SQLiteDatabase rdb = saveDocument.getReadableDatabase();
        SQLiteDatabase wdb = saveDocument.getWritableDatabase();

        Cursor c = rdb.query(SaveDocument.TagsTable.NAME,
                null,
                SaveDocument.TagsTable.COLUMN_NAME_NAME + " = ?",
                new String[]{name}, null, null, null);

        c.moveToFirst();
        Tag tag = new Tag();
        if(c.isAfterLast()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(SaveDocument.TagsTable.COLUMN_NAME_NAME, name);
            long insertedId = wdb.insert(SaveDocument.TagsTable.NAME, null, contentValues);
            tag.setId((int) insertedId);
            tag.setName(name);
            android.util.Log.i(TAG, "Inserted tag: " + insertedId);
        }else {
            tag.setId(c.getInt(c.getColumnIndex(SaveDocument.TagsTable._ID)));
            tag.setName(c.getString(c.getColumnIndex(SaveDocument.TagsTable.COLUMN_NAME_NAME)));
            android.util.Log.i(TAG, "Found tag: " + tag.getId());
        }

        return tag;
    }

    @Override
    protected void onPostExecute(Tag tag) {
        listener.tagAdded(tag);
    }
}

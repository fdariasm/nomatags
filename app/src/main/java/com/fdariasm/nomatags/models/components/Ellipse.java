package com.fdariasm.nomatags.models.components;

import com.fdariasm.nomatags.visitors.ShapeVisitor;

public class Ellipse extends BaseShape{

	private Point start;
	private Point end;

	public Ellipse(long seq) {
		super(seq);
	}

	@Override
	public void accept(ShapeVisitor visitor) {
		visitor.visitEllipse(this);
	}

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;

        if(!shapeRect.isEmpty())
            expandArea(start);
        else{
            initArea(start);
        }
        notifyChange(start);
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
        expandArea(end);
        notifyChange(end);
    }

}

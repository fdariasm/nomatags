package com.fdariasm.nomatags.models;

import android.util.Log;

import com.fdariasm.nomatags.events.DocChangedEvent;
import com.fdariasm.nomatags.events.DocChangedEvent.ChangeType;
import com.fdariasm.nomatags.listeners.DocumentListener;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.visitors.ShapeVisitor;

import java.util.LinkedList;

public class Current {
	
	private DocumentListeners mDocListeners = new DocumentListeners();
	private LinkedList<BaseShape> currentShapes = new LinkedList<BaseShape>();
	
	private Object selLock = new Object();
	private Rectangle userSelection = new Rectangle();
	
	public void currentShape(BaseShape shape, Point startPoint){
		
		currentShapes.add(shape);
		//shape.addChangedListener(this);
		
		DocChangedEvent event = new DocChangedEvent();
		event.setType(ChangeType.NEW);
		event.setShape(shape);
		event.setChangedPoint(startPoint);
		mDocListeners.notifyListeners(event);
	}

	synchronized public void clear(){
		if(!currentShapes.isEmpty()){
			//for(BaseShape shape : currentShapes)
			//	shape.removeChangedListener(this);
			currentShapes.clear();
		}
		DocChangedEvent event = new DocChangedEvent();
		event.setType(ChangeType.OTHER);
		mDocListeners.notifyListeners(event);
	}
	
	public void accept(ShapeVisitor visitor){
		if(!currentShapes.isEmpty())
			for(BaseShape shape : currentShapes)
				shape.accept(visitor);
	}
	
	public void addListener(DocumentListener listener){
		mDocListeners.addListener(listener);
	}
	
	public void removeListener(DocumentListener listener) {
		mDocListeners.removeListener(listener);
	}
	
	public void areaChanged(Point start, Point end){
		DocChangedEvent change = new DocChangedEvent();
		change.setType(ChangeType.CHANGENEW);
		change.setShape(currentShapes.getLast());
		change.setStartChange(start);
		change.setEndChange(end);
		mDocListeners.notifyListeners(change);
	}

	public Rectangle getUserSelection() {
		synchronized (selLock) {
			return userSelection;
		}
	}

	public void setUserSelection(Rectangle userSelection) {
		synchronized (selLock) {
			this.userSelection = userSelection;
		}
		Log.d("Current", "Set user selection "+ userSelection);

		if(!userSelection.isEmpty()){
			DocChangedEvent change = new DocChangedEvent();
			change.setType(ChangeType.SELECTION);
			change.setSelection(userSelection);
			mDocListeners.notifyListeners(change);
		}
	}

}

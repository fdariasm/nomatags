package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.visitors.MoveShapeVisitor;

public class MoveAction extends Action{

	private float dx;
	private float dy;

	public MoveAction(AppState appState, float dx, float dy) {
		super(appState);
		this.dx = dx;
		this.dy = dy;
	}

	@Override
	public void execute() {
		moveSelection(dx, dy);
	}

	@Override
	public void unexecute() {
		moveSelection(-dx, -dy);
	}

	private void moveSelection(float dx, float dy){
		MoveShapeVisitor visitor = new MoveShapeVisitor(dx, dy);
		ShapeSelection selection = mDocument.getCurrentSelection();
		for(BaseShape shape : selection.getShapes())
			shape.accept(visitor);
		Rectangle rect = selection.getRect();
		rect.offset(dx, dy);
		selection.getTransformPoint().movePoint(dx, dy);
	}
}

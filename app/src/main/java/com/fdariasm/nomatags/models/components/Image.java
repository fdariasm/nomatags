package com.fdariasm.nomatags.models.components;

import android.graphics.Bitmap;

import com.fdariasm.nomatags.visitors.ShapeVisitor;

public class Image extends BaseShape{

	private Bitmap mImage;
	private Point mPosition;
	
	
	public Image(long seq, Bitmap bitmap, Point position) {
		super(seq);
		mPosition = position;
		mImage = bitmap;
		updateRect();
	}

	private synchronized void updateRect() {
		float x = mPosition.getX();
		float y = mPosition.getY();
		
		shapeRect = new Rectangle(x, y, 
				x + mImage.getWidth(), 
				y + mImage.getHeight());
	}

	@Override
	public void accept(ShapeVisitor visitor) {
		visitor.visitImage(this);
	}

	public Bitmap getImage() {
		return mImage;
	}

	public void setImage(Bitmap mImage) {
		this.mImage = mImage;
		updateRect();
	}

	public Point getPosition() {
		return mPosition;
	}

	public void setPosition(Point position) {
		this.mPosition = position;
		updateRect();
		notifyChange(position);
	}

}

package com.fdariasm.nomatags.models;

import com.fdariasm.nomatags.events.DocChangedEvent;
import com.fdariasm.nomatags.listeners.DocumentListener;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class DocumentListeners  implements Serializable {
	
	private Set<DocumentListener> mListeners = new HashSet<DocumentListener>();

	public void addListener(DocumentListener listener){
		mListeners.add(listener);
	}
	
	public void removeListener(DocumentListener listener){
		mListeners.remove(listener);
	}
	
	public void notifyListeners(DocChangedEvent event){
		for(DocumentListener listener : mListeners){
			listener.documentChanged(event);
		}
	}

}

package com.fdariasm.nomatags.models.process.db_taks;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.fdariasm.nomatags.controllers.actions.ActionHandler;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Curve;
import com.fdariasm.nomatags.models.components.Ellipse;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Line;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Square;
import com.fdariasm.nomatags.models.components.Tag;
import com.fdariasm.nomatags.models.process.SaveDocument;
import com.fdariasm.nomatags.models.process.SaveDocument.BaseShapeTable;
import com.fdariasm.nomatags.models.process.SaveDocument.DocumentTable;
import com.fdariasm.nomatags.models.process.SaveDocument.LSETable;
import com.fdariasm.nomatags.models.process.SaveDocument.OnDocumentLoadedListener;
import com.fdariasm.nomatags.models.process.SaveDocument.Types;
import com.fdariasm.nomatags.strategies.DrawShapeStrategy;
import com.fdariasm.nomatags.strategies.DrawStrategyFactory;
import com.fdariasm.nomatags.strategies.android.AndroidStrategyFactory;
import com.fdariasm.nomatags.utils.Conversion;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Created by fdariasm on 03/11/2015.
 */
public class LoadDocumentTask extends AsyncTask<Integer, Void, DocumentShapes> {

    private final OnDocumentLoadedListener listener;
    private SaveDocument saveDocument;

    protected static final float TOUCH_TOLERANCE = 3;

    public LoadDocumentTask(OnDocumentLoadedListener listener, SaveDocument saveDocument){
        this.listener = listener;
        this.saveDocument = saveDocument;
    }

    @Override
    protected DocumentShapes doInBackground(Integer... params) {
        Integer docId = params[0];

        SQLiteDatabase rdb = saveDocument.getReadableDatabase();
        Cursor qc = rdb.query(DocumentTable.NAME,
                null,
                DocumentTable._ID + " = ? ",
                new String[]{docId.toString()},
                null, null, null);

        qc.moveToFirst();
        if(qc.isAfterLast())
            return null;


        DocumentShapes document = new DocumentShapes();
        document.setId(new Long(docId));
        document.setName(qc.getString(qc.getColumnIndex(DocumentTable.COLUMN_NAME_NAME)));
        document.setCreationDate(Conversion.loadDate(qc, qc.getColumnIndex(DocumentTable.COLUMN_NAME_CREATION_DATE)));
        document.setCurrentSeq(qc.getInt(qc.getColumnIndex(DocumentTable.COLUMN_NAME_CURRENT_SEQ)));
        document.setPages(qc.getInt(qc.getColumnIndex(DocumentTable.COLUMN_NAME_PAGES)));

        document.setTags(loadTags(document, rdb));

        loadShapes(document, rdb);
        qc.close();

        return document;
    }

    private Set<Tag> loadTags(DocumentShapes document, SQLiteDatabase rdb) {

        Set<Tag> tags = new HashSet<Tag>();

        Cursor dtcursor = rdb.query(SaveDocument.DocumentTagsTable.NAME,
                null,
                SaveDocument.DocumentTagsTable.COLUMN_NAME_DOCID + " = ?",
                new String[]{document.getId().toString()},
                null, null, null);

        while(dtcursor.moveToNext()){
            int tagId = dtcursor.getInt(dtcursor.getColumnIndex(SaveDocument.DocumentTagsTable.COLUMN_NAME_TAGID));
            Cursor tagCursor = rdb.query(SaveDocument.TagsTable.NAME,
                    null,
                    SaveDocument.TagsTable._ID + " = ?",
                    new String[]{"" + tagId}, null, null, null);

            tagCursor.moveToFirst();

            if(tagCursor.isAfterLast()) continue;

            Tag tag = new Tag();
            tag.setId(tagCursor.getInt(tagCursor.getColumnIndex(SaveDocument.TagsTable._ID)));
            tag.setName(tagCursor.getString(tagCursor.getColumnIndex(SaveDocument.TagsTable.COLUMN_NAME_NAME)));

            tags.add(tag);
        }

        return tags;
    }

    private void loadShapes(DocumentShapes document, SQLiteDatabase rdb) {
        Cursor qc = rdb.query(BaseShapeTable.NAME,
                null,
                BaseShapeTable.COLUMN_NAME_DOCUMENT + " = ?",
                new String[]{document.getId().toString()},
                null, null, null);

        LinkedList<BaseShape> loadedShapes = new LinkedList<BaseShape>();
        while(qc.moveToNext()) {
            Integer type = qc.getInt(qc.getColumnIndex(BaseShapeTable.COLUMN_NAME_TYPE));
            if(type.equals(Types.LINE.id)){
                Line line = loadLine(rdb, qc);
                loadedShapes.add(line);
            }else if(type.equals(Types.SQUARE.id)){
                loadedShapes.add(loadSquare(rdb, qc));
            }else if(type.equals(Types.ELLIPSE.id)) {
                loadedShapes.add(loadEllipse(rdb, qc));
            }else if(type.equals(Types.CURVE.id)){
                loadedShapes.add(loadCurve(rdb, qc));
            }else if(type.equals(Types.IMAGE.id)){
                Image img = loadImage(rdb, qc);
                if(null != img)
                    loadedShapes.add(img);
            }
        }
        qc.close();

        document.addAll(loadedShapes);

    }

    private BaseShape loadEllipse(SQLiteDatabase rdb, Cursor shapeCursor) {
        int seq = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_DOCUMENT_SEQ));
        Ellipse ellipse = new Ellipse(seq);

        ellipse.setStrokeWidth(shapeCursor.getFloat(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_STROKE)));
        ellipse.setColor(shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_COLOR)));

        Integer shapeId = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable._ID));

        Cursor lcs = rdb.query(LSETable.NAME,
                null,
                LSETable.COLUMN_NAME_SHAPE + " = ? ",
                new String[]{shapeId.toString()},
                null, null, null);

        lcs.moveToFirst();

        Point start = new Point();
        start.setX(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_START_X)));
        start.setY(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_START_Y)));

        Point end = new Point();
        end.setX(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_END_X)));
        end.setY(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_END_Y)));

        ellipse.setStart(start);
        ellipse.setEnd(end);

        lcs.close();
        return ellipse;

    }


    private Image loadImage(SQLiteDatabase rdb, Cursor shapeCursor) {
        int seq = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_DOCUMENT_SEQ));

        Integer shapeId = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable._ID));

        Cursor lcs = rdb.query(SaveDocument.ImageTable.NAME,
                null,
                SaveDocument.ImageTable.COLUMN_NAME_SHAPE + " = ? ",
                new String[]{shapeId.toString()},
                null, null, null);

        lcs.moveToFirst();

        Point position = new Point();
        position.setX(lcs.getFloat(lcs.getColumnIndex(SaveDocument.ImageTable.COLUMN_NAME_POS_X)));
        position.setY(lcs.getFloat(lcs.getColumnIndex(SaveDocument.ImageTable.COLUMN_NAME_POS_Y)));

        String filePath = lcs.getString(lcs.getColumnIndex(SaveDocument.ImageTable.COLUMN_NAME_IMAGE_URI));

        File file = new File(filePath);
        if(!file.exists())
            return null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

        Image image = new Image(seq, bitmap, position);

        image.setStrokeWidth(shapeCursor.getFloat(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_STROKE)));
        image.setColor(shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_COLOR)));

        lcs.close();
        return image;
    }


    private Line loadLine(SQLiteDatabase rdb, Cursor shapeCursor) {
        int seq = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_DOCUMENT_SEQ));
        Line line = new Line(seq);

        line.setStrokeWidth(shapeCursor.getFloat(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_STROKE)));
        line.setColor(shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_COLOR)));

        Integer shapeId = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable._ID));

        Cursor lcs = rdb.query(LSETable.NAME,
                null,
                LSETable.COLUMN_NAME_SHAPE + " = ? ",
                new String[]{shapeId.toString()},
                null, null, null);

        lcs.moveToFirst();

        Point start = new Point();
        start.setX(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_START_X)));
        start.setY(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_START_Y)));

        Point end = new Point();
        end.setX(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_END_X)));
        end.setY(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_END_Y)));

        line.setStart(start);
        line.setEnd(end);

        lcs.close();
        return line;
    }

    private Curve loadCurve(SQLiteDatabase rdb, Cursor shapeCursor) {

        Integer shapeId = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable._ID));
        int shapeSeq = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_DOCUMENT_SEQ));

        Cursor lcs = rdb.query(SaveDocument.CurveTable.NAME,
                null,
                SaveDocument.CurveTable.COLUMN_NAME_SHAPE + " = ? ",
                new String[]{shapeId.toString()},
                null, null, null);

        AndroidStrategyFactory factory = AndroidStrategyFactory.getFactory();
        CurveFromDB dbCurve = new CurveFromDB(factory, shapeSeq);

        int i = 0;
        int count = lcs.getCount();

        while(lcs.moveToNext()){
            Point point = new Point();
            point.setX(lcs.getFloat(lcs.getColumnIndex(SaveDocument.CurveTable.COLUMN_NAME_POS_X)));
            point.setY(lcs.getFloat(lcs.getColumnIndex(SaveDocument.CurveTable.COLUMN_NAME_POS_Y)));

            if(i == 0)
                dbCurve.doDown(point);
            else if(i == (count - 1))
                dbCurve.doUp(point);
            else
                dbCurve.doMove(point);
            i++;
        }

        lcs.close();

        Curve curve = dbCurve.getCurve();

        curve.setStrokeWidth(shapeCursor.getFloat(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_STROKE)));
        curve.setColor(shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_COLOR)));

        return curve;
    }

    private Square loadSquare(SQLiteDatabase rdb, Cursor shapeCursor) {
        int seq = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_DOCUMENT_SEQ));
        Square square = new Square(seq);

        square.setStrokeWidth(shapeCursor.getFloat(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_STROKE)));
        square.setColor(shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable.COLUMN_NAME_COLOR)));

        Integer shapeId = shapeCursor.getInt(shapeCursor.getColumnIndex(BaseShapeTable._ID));

        Cursor lcs = rdb.query(LSETable.NAME,
                null,
                LSETable.COLUMN_NAME_SHAPE + " = ? ",
                new String[]{shapeId.toString()},
                null, null, null);

        lcs.moveToFirst();

        Point start = new Point();
        start.setX(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_START_X)));
        start.setY(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_START_Y)));

        Point end = new Point();
        end.setX(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_END_X)));
        end.setY(lcs.getFloat(lcs.getColumnIndex(LSETable.COLUMN_NAME_END_Y)));

        square.setStart(start);
        square.setEnd(end);

        lcs.close();
        return square;
    }

    @Override
    protected void onPostExecute(DocumentShapes documentShapes) {
        listener.updateState(documentShapes);
    }

    private class CurveFromDB{
        private Curve mCurve;
        private DrawShapeStrategy mStrategy;

        private DrawStrategyFactory mFactory;
        private int sequence;

        private Point p1;
        private Point p0;

        public CurveFromDB(DrawStrategyFactory factory, int sequence){
            this.mFactory = factory;
            this.sequence = sequence;
        }

        public void doDown(Point docPoint){
            p0 = p1 = docPoint;
            mStrategy = mFactory.getDrawStrategy();
            mStrategy.moveTo(docPoint);
            mCurve = new Curve(sequence, mStrategy);

            mCurve.addPoint(docPoint);
        }

        public void doMove(Point docPoint){
            move(docPoint);
        }

        protected void move(Point current){
            if(pointInTolerance(current)){
                if(null != p0){
                    cubicSpline(current, false);
                }
                p0 = p1;
                p1 = current;
                mCurve.addPoint(current);
            }
        }

        public void doUp(Point docPoint){
            if(docPoint.equals(p0)){
                Point singlePoint = getSinglePoint(docPoint);
                mStrategy.lineTo(singlePoint);
                mCurve.addPoint(singlePoint);

                return;
            }
            cubicSpline(docPoint, true);
            mCurve.addPoint(docPoint);
        }

        protected Point getSinglePoint(Point point){
            float x = point.getX() + ActionHandler.SINGLEPOINTSIZE;
            float y = point.getY() + ActionHandler.SINGLEPOINTSIZE;
            return new Point(x, y);
        }

        protected void cubicSpline(Point p2, boolean lastPoint){

            float x = (2*p0.getX() + p1.getX())/3;
            float y = (2*p0.getY() + p1.getY())/3;
            Point c1 = new Point(x, y);

            x = (p0.getX() + 2*p1.getX())/3;
            y = (p0.getY() + 2*p1.getY())/3;
            Point c2 = new Point(x, y);

            x = (2*p1.getX() + p2.getX())/3;
            y = (2*p1.getY() + p2.getY())/3;
            Point c3 = new Point(x, y);

            x = (p1.getX() + 2*p2.getX())/3;
            y = (p1.getY() + 2*p2.getY())/3;
            Point c4 = new Point(x, y);

            x = (c2.getX() + c3.getX())/2;
            y = (c2.getY() + c3.getY())/2;
            Point s1 = new Point(x, y);


            mStrategy.cubicTo(c1, c2, s1);

            if(lastPoint){
                mStrategy.cubicTo(c3, c4, p2);
            }
        }

        protected boolean pointInTolerance(Point p){
            float dx = Math.abs(p.getX() - p1.getX());
            float dy = Math.abs(p.getY() - p1.getY());
            return dx > TOUCH_TOLERANCE || dy > TOUCH_TOLERANCE;
        }

        public Curve getCurve(){
            return mCurve;
        }
    }
}

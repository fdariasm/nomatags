package com.fdariasm.nomatags.models;

import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.models.components.Rectangle;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ShapeSelection implements Serializable{
	
	private Rectangle rect = new Rectangle();
	
	private List<BaseShape> shapes = new LinkedList<BaseShape>();
	
	private Point transformPoint;

	public Rectangle getRect() {
		return rect;
	}

	public void setRect(Rectangle rect) {
		this.rect = rect;
		float x = (rect.left + rect.right) / 2;
		transformPoint = new Point(x, 
				rect.bottom + Presentation.TRANSFORMDISTANCE);
	}

	public List<BaseShape> getShapes() {
		return shapes;
	}

	public void setShapes(List<BaseShape> shapes) {
		this.shapes = shapes;
	}
	
	public boolean isEmpty(){
		return shapes.isEmpty();
	}

	public Point getTransformPoint() {
		return transformPoint;
	}

	public void setTransformPoint(Point transformPoint) {
		this.transformPoint = transformPoint;
	}

}

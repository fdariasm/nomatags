package com.fdariasm.nomatags.models.process;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.Presentation;
import com.fdariasm.nomatags.models.actions.Action;
import com.fdariasm.nomatags.models.actions.CreateAction;
import com.fdariasm.nomatags.models.components.Image;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.utils.BitmapUtils;

public class ImportImage extends AsyncTask<String, Void, Action>{
		
	private DocumentShapes document;
	private Presentation presentation;
	private AppState appState;
	
	private static final int IMAGESAMPLE = 3;

	public ImportImage(AppState appState) {
		this.appState = appState;
		document = appState.getDocument();
		presentation = appState.getPresentation();
	}
	
	public void startImport(Intent data, ContentResolver resolver){
        Uri selectedImage = data.getData();
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        Cursor cursor = resolver.query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        
        execute(picturePath);
	}

	@Override
	protected Action doInBackground(String... params) {
		String path = params[0];
		int reqWidth = presentation.getScreenWidth() / IMAGESAMPLE;
		int reqHeight = presentation.getScreenHeight() / IMAGESAMPLE;
		Bitmap result = BitmapUtils.decodeSampledBitmapFromPath(path, reqWidth, reqHeight);
		if(null != result){
			Image image = new 
					Image(document.getNextSeq(), result, 
							new Point(0,presentation.getVertOffset()));
			CreateAction createImage = new CreateAction(appState, image);
						
			return createImage;
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Action action) {
		if(null != action){
			action.execute();
			appState.getUndoManager()
					.pushUndo(action);
		}
	}
}

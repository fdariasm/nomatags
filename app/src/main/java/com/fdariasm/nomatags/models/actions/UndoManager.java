package com.fdariasm.nomatags.models.actions;

import java.util.LinkedList;

public class UndoManager {
	
	private LinkedList<Action> mUndoActions;
	private LinkedList<Action> mRedoActions;

	private static UndoManager INSTANCE;
	
	private UndoManager() {
		mUndoActions = new LinkedList<Action>();
		mRedoActions = new LinkedList<Action>();
	}
	
	public static UndoManager getManager(){
		if( null == INSTANCE )
			INSTANCE = new UndoManager();
		
		return INSTANCE;
	}
	
	public synchronized void pushUndo(Action action){
		mUndoActions.push(action);
		mRedoActions.clear();
	}
	
	public synchronized Action popUndo(){
		Action pop = mUndoActions.pop();
		mRedoActions.push(pop);
		return pop;
	}
	
	public synchronized Action popRedo(){
		Action pop = mRedoActions.pop();
		mUndoActions.push(pop);
		return pop;
	}
	
	public boolean isUndoEmpty(){
		return mUndoActions.isEmpty();
	}
	
	public boolean isRedoEmpty(){
		return mRedoActions.isEmpty();
	}
}

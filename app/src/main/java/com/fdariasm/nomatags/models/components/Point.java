package com.fdariasm.nomatags.models.components;


import java.io.Serializable;

public class Point  implements Serializable {

	private float x;
	private float y;
	
	public Point() {
	}

	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Point(Point oPoint) {
		this.x = oPoint.getX();
		this.y = oPoint.getY();
	}
	
	public Point(Point oPoint, float vertOffset) {
		this.x = oPoint.getX();
		this.y = oPoint.getY() + vertOffset;
	}
	
	public Point(float x, float y, float hOffset, float vertOffset) {
		this.x = x + hOffset;
		this.y = y + vertOffset;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	
	@Override
	public String toString() {
		return "x: " + x + " y: " + y;
	}
	
	public float distanceTo(Point to){
		double a = Math.pow(this.x-to.x, 2);
		double b = Math.pow(this.y-to.y, 2);
		return (float) Math.sqrt(a + b);
	}
	
		
	public Point moveNewPoint(float dd){
		return new Point(x + dd, y + dd);
	}
	
	public Point movePoint(float dx, float dy){
		this.x += dx;
		this.y += dy;
		return this;
	}
	
	public void rotate(Point reference, double cosA, double sinA){
		
		x -= reference.getX();
		y -= reference.getY();
		float newX = (float) (x * cosA - y * sinA);
		float newY = (float) (x * sinA + y * cosA);
		
		x = newX + reference.getX();
		y = newY + reference.getY();
	}
	
	public void scale(float scaleFactor, Point reference){
		x -= reference.getX();
		y -= reference.getY();
		x *= scaleFactor;
		y *= scaleFactor;
		x += reference.getX();
		y += reference.getY();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		
		return x == other.x && y == other.y;
	}
}

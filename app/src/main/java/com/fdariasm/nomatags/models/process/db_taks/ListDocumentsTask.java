package com.fdariasm.nomatags.models.process.db_taks;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.fdariasm.nomatags.models.process.SaveDocument;
import com.fdariasm.nomatags.models.process.SaveDocument.DocumentTable;
import com.fdariasm.nomatags.models.process.SaveDocument.OnCursorResultsListener;

/**
 * Created by fdariasm on 03/11/2015.
 */
public class ListDocumentsTask extends AsyncTask<ListDocumentsTask.QueryDoc, Void, Cursor> {

    private OnCursorResultsListener listener;

    private SaveDocument saveDocument;

    public ListDocumentsTask(OnCursorResultsListener listener,
                             SaveDocument saveDocument){
        this.listener = listener;
        this.saveDocument = saveDocument;
    }

    @Override
    protected Cursor doInBackground(ListDocumentsTask.QueryDoc... params) {

        QueryDoc q = params[0];

        SQLiteDatabase rdb = saveDocument.getReadableDatabase();
        Cursor cursor = rdb.query(DocumentTable.NAME, null,
                q.column, q.value, null, null,
                DocumentTable.COLUMN_NAME_CREATION_DATE + " DESC");
        cursor.getCount();
        return cursor;
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        if(null != cursor)
            listener.showResults(cursor);
    }

    public static class QueryDoc{
        public final String column;

        public final String[] value;

        public QueryDoc(String column, String[] value) {
            this.column = column;
            this.value = value;
        }
    }
}

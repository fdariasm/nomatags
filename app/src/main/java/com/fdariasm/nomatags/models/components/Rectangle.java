package com.fdariasm.nomatags.models.components;

import android.annotation.SuppressLint;

import java.io.Serializable;


public class Rectangle  implements Serializable {

	public float top;
	public float left;
	public float right;
	public float bottom;
	
	public Rectangle(){}
	
	public Rectangle(float left, float top, float right, float bottom) {
		this.top = top;
		this.left = left;
		this.right = right;
		this.bottom = bottom;
	}
	
	@SuppressLint("DefaultLocale")
	@Override
	public String toString() {
		return String.format("Top: %f, Left:%f, right: %f, bottom:%f", 
				top, left, right, bottom);
	}

	public boolean contains(float x, float y){
		return x > left && x < right && y > top && y < bottom;
	}
	
	public boolean isEmpty(){
		
		return (left >= right) || (top >= bottom);
	}
	
    public boolean intersect(float left, float top, float right, float bottom) {
        return (this.left < right && left < this.right
                && this.top < bottom && top < this.bottom);
    }
    public boolean intersect(Rectangle r) {
        return intersect(r.left, r.top, r.right, r.bottom);
    }
    
	public boolean contains(float left, float top, float right, float bottom) {
		// check for empty first
		return this.left < this.right && this.top < this.bottom
				// now check for containment
				&& this.left <= left && this.top <= top && this.right >= right
				&& this.bottom >= bottom;
	}
	
	public boolean contains(Rectangle r) {
		// check for empty first
		return this.left < this.right && this.top < this.bottom
				// now check for containment
				&& left <= r.left && top <= r.top
				&& right >= r.right && bottom >= r.bottom;
	}
	
	public void offset(float dx, float dy){
		left += dx;
		right += dx;
		top += dy;
		bottom += dy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(bottom);
		result = prime * result + Float.floatToIntBits(left);
		result = prime * result + Float.floatToIntBits(right);
		result = prime * result + Float.floatToIntBits(top);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rectangle other = (Rectangle) obj;
		if (Float.floatToIntBits(bottom) != Float.floatToIntBits(other.bottom))
			return false;
		if (Float.floatToIntBits(left) != Float.floatToIntBits(other.left))
			return false;
		if (Float.floatToIntBits(right) != Float.floatToIntBits(other.right))
			return false;
		if (Float.floatToIntBits(top) != Float.floatToIntBits(other.top))
			return false;
		return true;
	}
}

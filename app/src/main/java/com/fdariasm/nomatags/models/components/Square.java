package com.fdariasm.nomatags.models.components;

import com.fdariasm.nomatags.visitors.ShapeVisitor;

public class Square extends BaseShape{

	public Square(long seq) {
		super(seq);
	}

	private Point start;
	
	public Point getStart() {
		return start;
	}

	public void setStart(Point start) {
		this.start = start;
		notifyChange(start);
	}

	public Point getEnd() {
		return end;
	}

	public void setEnd(Point end) {
		this.end = end;
		notifyChange(start);
	}

	private Point end;
	
	@Override
	public void accept(ShapeVisitor visitor) {
		visitor.visitSquare(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + (int) (seqNumber ^ (seqNumber >>> 32));
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Square other = (Square) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (seqNumber != other.seqNumber)
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		return true;
	}
	
	

}

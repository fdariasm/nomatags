package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.events.ToolChangedEvent;

public class SelectAction extends Action{

	private ShapeSelection prevSelection;
	private ShapeSelection currSelection;
	
	
	public SelectAction(AppState appState,
			ShapeSelection prevSelection, 
			ShapeSelection currSelection) {
		super(appState);
		
		this.prevSelection = prevSelection;
		this.currSelection = currSelection;
	}

	@Override
	public void execute() {
		mDocument.setCurrentSelection(currSelection);
		toolState.setTool(ToolChangedEvent.DrawingTool.SELECT);
	}

	@Override
	public void unexecute() {
		mDocument.setCurrentSelection(prevSelection);
		toolState.setTool(prevTool);
	}

}

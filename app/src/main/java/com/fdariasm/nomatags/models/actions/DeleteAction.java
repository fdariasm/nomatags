package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.ToolState;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.events.ToolChangedEvent;

public class DeleteAction extends Action{

	private ShapeSelection shapeSelection;
	
	private ToolChangedEvent.DrawingTool prevTool;
	private ToolState toolState;
	
	public DeleteAction(AppState appState,
			ShapeSelection shapeSelection) {
		super(appState);
		this.shapeSelection = shapeSelection;
		
		toolState = appState.getToolState();
		prevTool = toolState.getTool();
	}

	@Override
	public void execute() {
		for(BaseShape shape : shapeSelection.getShapes()){
			mDocument.remove(shape);
		}
		mDocument.setCurrentSelection(new ShapeSelection());
	}

	@Override
	public void unexecute() {
		for(BaseShape shape : shapeSelection.getShapes()){
			mDocument.add(shape);
		}
		mDocument.setCurrentSelection(shapeSelection);
		toolState.setTool(prevTool);
	}


}

package com.fdariasm.nomatags.models;

import java.util.LinkedList;

import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Rectangle;
import com.fdariasm.nomatags.visitors.InterceptVisitor;
import com.fdariasm.nomatags.visitors.ShapeVisitor;
import com.fdariasm.nomatags.visitors.VisiblesVisitor;

public class VisibleShapes {
	
	static final String TAG = VisibleShapes.class.getSimpleName();
	private LinkedList<BaseShape> visible;
	private DocumentShapes document;
	private VisiblesVisitor visibleVisitor;
	private InterceptVisitor interceptVisitor;
		
	public VisibleShapes(AppState appState, Rectangle visibleArea) {
		
		document = appState.getDocument();
		updateVisibles(visibleArea);
	}

	public synchronized LinkedList<BaseShape> getVisibles() {
		return visible;
	}

	public synchronized void setVisibles(LinkedList<BaseShape> visibles) {
		this.visible = visibles;
	}
	
	public synchronized void add(BaseShape shape){
		shape.accept(interceptVisitor);
	}
	
	public synchronized void remove(BaseShape shape){
		visible.remove(shape);
	}
	
	public synchronized void accept(ShapeVisitor visitor){
		for(BaseShape shape : visible){
			shape.accept(visitor);
		}
	}
	
	public synchronized void updateVisibles( Rectangle visibleArea ){
		visibleVisitor = new VisiblesVisitor();
		interceptVisitor = new InterceptVisitor(visibleArea, visibleVisitor);
		document.accept(interceptVisitor);
		visible = visibleVisitor.getVisible();
	}
}

package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;

public class CompositeAction extends Action{

	private Action[] actions;
	
	public CompositeAction(AppState appState,
			Action... actions) {
		super(appState);
		this.actions = actions;
	}

	@Override
	public void execute() {
		for(Action action : actions)
			action.execute();
	}

	@Override
	public void unexecute() {
		for(Action action : actions)
			action.unexecute();
	}

}

package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.ToolState;
import com.fdariasm.nomatags.events.ToolChangedEvent;

public abstract class Action {
	
	protected AppState appState;
	protected DocumentShapes mDocument;
	
	protected ToolChangedEvent.DrawingTool prevTool;
	protected ToolState toolState;
	
	public Action(AppState appState) {
		this.appState = appState;
		mDocument = appState.getDocument();
		toolState = appState.getToolState();
		prevTool = toolState.getTool();
	}

	protected void restoreState(){
		ToolChangedEvent.DrawingTool tool = toolState.getTool();
		toolState.setTool(prevTool);
		prevTool = tool;
	}

	public abstract void execute();
	
	public abstract void unexecute();
}

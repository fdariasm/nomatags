package com.fdariasm.nomatags.models.actions;

import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.ShapeSelection;
import com.fdariasm.nomatags.models.components.BaseShape;
import com.fdariasm.nomatags.models.components.Point;
import com.fdariasm.nomatags.visitors.AreaSelectionVisitor;
import com.fdariasm.nomatags.visitors.RotateShapeVisitor;
import com.fdariasm.nomatags.visitors.ScaleShapeVisitor;

public class TransformAction extends Action{
	
	private Point centerPoint;
	private float scaleFactor;
	private float angle;

	public TransformAction(AppState appState, 
			Point centerPoint, float scaleFactor, float angle) {
		super(appState);
		this.centerPoint = centerPoint;
		this.scaleFactor = scaleFactor;
		this.angle = angle;
	}

	@Override
	public void execute() {
		transformSelection(scaleFactor, angle);
	}

	@Override
	public void unexecute() {
		transformSelection(1 / scaleFactor, -angle);
	}
	
	private void transformSelection(float scaleFactor, float angle){
		ScaleShapeVisitor scaleVisitor = new ScaleShapeVisitor(scaleFactor, centerPoint);
		RotateShapeVisitor rotateVisitor = new RotateShapeVisitor( angle, centerPoint );
		AreaSelectionVisitor areaVisitor = new AreaSelectionVisitor();
		ShapeSelection selection = mDocument.getCurrentSelection();
		for(BaseShape shape : selection.getShapes()){
			shape.accept(scaleVisitor);
			shape.accept(rotateVisitor);
			shape.accept(areaVisitor);
		}
		selection.setRect(areaVisitor.getSelectedArea());
	}

}

package com.fdariasm.nomatags.models.components;

import java.util.LinkedList;

import com.fdariasm.nomatags.strategies.DrawShapeStrategy;
import com.fdariasm.nomatags.visitors.ShapeVisitor;

public class Curve extends BaseShape{
	
	private DrawShapeStrategy mStrategy;

	private LinkedList<Point> mPoints = new LinkedList<Point>();
	
	public Curve(long seq,  DrawShapeStrategy strategy ) {
		super(seq);
		mStrategy = strategy;
	}
	
	@Override
	synchronized public void accept(ShapeVisitor visitor) {
		visitor.visitCurve(this);
	}

	public DrawShapeStrategy getStrategy() {
		return mStrategy;
	}

	public LinkedList<Point> getPoints() {
		return mPoints;
	}

	public void setPoints(LinkedList<Point> points) {
		this.mPoints = points;
		notifyChange(points.getLast());
	}
	
	synchronized public void addPoint(Point point){
		mPoints.add(point);
		notifyChange(point);
	}

}

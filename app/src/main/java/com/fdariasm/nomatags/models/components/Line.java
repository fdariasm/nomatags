package com.fdariasm.nomatags.models.components;


import com.fdariasm.nomatags.visitors.ShapeVisitor;

public class Line extends BaseShape {

	public Line(long seq) {
		super(seq);
	}

	private Point start;
	
	private Point end;
	
	@Override
	public void accept(ShapeVisitor visitor) {
		visitor.visitLine(this);
	}

	public Point getStart() {
		return start;
	}

	public void setStart(Point start) {
		this.start = start;
		
		if(!shapeRect.isEmpty())
			expandArea(start);
		else{
			initArea(start);
		}
		notifyChange(start);
	}

	public Point getEnd() {
		return end;
	}

	public void setEnd(Point end) {
		this.end = end;
		expandArea(end);
		notifyChange(end);
	}
}

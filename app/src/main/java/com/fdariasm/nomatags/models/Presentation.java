package com.fdariasm.nomatags.models;

import android.util.Log;

import com.fdariasm.nomatags.events.PresentationEvent;
import com.fdariasm.nomatags.listeners.PresentationListener;

import java.util.HashSet;
import java.util.Set;

public class Presentation {

	private static final String TAG = Presentation.class.getSimpleName();

	private volatile Float vOffset = 0f;
	private volatile Float hOffset = 0f;

//	private float scaleX = 1;
	private float scale = 1.f;

	private float pageWidth = 800;
	private float pageHeight = 600;

	private int pages = 1;

	private Float multiplierY = 1f;
	private Float multiplierX = 1f;

	private int screenWidth;
	private int screenHeight;

	private float vUpperLimit = 0;
	private float vLowerLimit;

	private float hUpperLimit = 0;
	private float hLowerLimit;

	private Boolean scrollingY = false;
	private Boolean scrollingX = false;


	public static final int VSCROLLWIDTH = 100;
	public static final int VSCROLLHEIGHT = 120;
	public static final float MARGIN = 2;

	public static final int HSCROLLWIDTH = 120;
	public static final int HSCROLLHEIGHT = 90;

	public static final float SCALELIMIT = 4.0f;
	public static final float GRIDSIZE = 30.f;
	public static final float ROWSIZE = 60.f;
	public static final float ROW_MARGIN = 15.f;
	public static final float TRANSFORMAREA = 40.f;
	public static final float TRANSFORMDISTANCE = 150.f;

	public final int LIMITERHEIGHT = 3;

	private final Object lock1 = new Object();
	private final Object lock2 = new Object();


	private Set<PresentationListener> mListeners = new HashSet<PresentationListener>();

	private float minScale;


	public void addListener(PresentationListener listener){
		mListeners.add(listener);
	}

	public void removeListener(PresentationListener listener){
		mListeners.remove(listener);
	}

	public void notifyListeners(PresentationEvent event){
		for(PresentationListener listener : mListeners){
			listener.presentationChanged(event);
		}
	}

	public float getVertOffset() {
		synchronized (lock1) {
			return vOffset * multiplierY;
		}
	}

	public Float getHOffset() {
		synchronized (lock1) {
			return hOffset * multiplierX;
		}
	}

	public void setHOffset(float hOffset) {
		synchronized (lock1) {
			this.hOffset = Math.max(hUpperLimit, Math.min(hOffset, hLowerLimit));
		}
		notifyListeners(new PresentationEvent());
	}

	public float getScrollHOffset(){
		synchronized (lock1) {
			return hOffset;
		}
	}

	public float getScrollVertOffset(){
		synchronized (lock1) {
			return vOffset;
		}
	}

	public void setVertOffset(float vertOffset) {
		synchronized (lock1) {
			this.vOffset = Math.max(vUpperLimit, Math.min(vertOffset, vLowerLimit));
		}
		notifyListeners(new PresentationEvent());
	}

	public float getVMultiplier() {
		return multiplierY;
	}


	public float getHMultiplier(){
		return multiplierX;
	}

	public int getScreenWidth() {
		return screenWidth;
	}

	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
		hLowerLimit = screenWidth - HSCROLLWIDTH - hUpperLimit;
	}

	public int getScreenHeight() {
		return screenHeight;
	}

	public void setScreenHeight(int screenHeight) {
		this.screenHeight = screenHeight;
		vLowerLimit = screenHeight - VSCROLLHEIGHT - vUpperLimit;
	}

	public float getVLowerLimit(){
		return vLowerLimit;
	}

	public float getVUpperLimit(){
		return hUpperLimit;
	}

	public float getHLowerLimit(){
		return hLowerLimit;
	}

	public float getHUpperLimit(){
		return vUpperLimit;
	}

	public boolean isVScrolling() {
		synchronized (lock2) {
			return scrollingY;
		}
	}

	public void setVScrolling(boolean isScrolling) {
		synchronized (lock2) {
			this.scrollingY = isScrolling;
		}
	}

	public float getScale() {
		synchronized (lock1) {
			return scale;
		}
	}

	public void setScale( float scale ) {
		scale = Math.max(minScale, Math.min(scale, SCALELIMIT));

		synchronized (lock1) {
			this.scale = scale;
			setVMultiplier();
			setHMultiplier();

		}
		notifyListeners(new PresentationEvent());
	}

	public void increaseScale(float scale){
		synchronized (lock1) {
			setScale(this.scale * scale);
		}

	}

	public void setPageMetrics(float width, float height){
		pageWidth = width;
		pageHeight = height;
		scale = 1.f;

		float minScaleX = (float)screenWidth / pageWidth;
		float minScaleY = (float)screenHeight / pageHeight;
		minScale =  Math.max(minScaleX, minScaleY);

		setVMultiplier();
		setHMultiplier();
	}

	public float getPageWidth(){
		return pageWidth;
	}

	public float getPageHeight(){
		return pageHeight;
	}

	public void setPages(int pages){
        synchronized (lock1) {
            this.pages = pages;
            setVMultiplier();
            vOffset = (pageHeight * (pages - 1)) / multiplierY;
        }
        notifyListeners(new PresentationEvent());
    }

	public void addNewPage(){
		synchronized (lock1) {
			pages += 1;
			setVMultiplier();
			vOffset = (pageHeight * (pages - 1)) / multiplierY;
		}
		notifyListeners(new PresentationEvent());
	}

	private void setVMultiplier(){
		multiplierY = (((pageHeight * pages) - (screenHeight * (1 / scale))) / vLowerLimit);
		if(multiplierY < 0 || Math.abs(multiplierY) < 2E-4 )
			multiplierY = 0f;

		Log.i(TAG, "MultiplierY: " + multiplierY);
	}

	private void setHMultiplier(){
		multiplierX = (pageWidth - (screenWidth * ( 1/ scale ))) / hLowerLimit ;
		if(multiplierX < 0 || Math.abs(multiplierX) < 2E-4 )
			multiplierX = 0f;
		Log.i(TAG, "MultiplierX: " + multiplierX);
	}

	public int getPages(){
		synchronized (lock1) {
			return pages;
		}
	}

	public int getCurrentPageNumber() {
		return (int) ((vOffset * multiplierY) / pageHeight) + 1;
	}

	public float getLimit(int pageNumber) {
		return (pageNumber * pageHeight) - (vOffset * multiplierY); // 
	}

	public float getRealLimit(int pageNumber) {
		return (pageNumber * pageHeight);
	}

	public float getCurrentLimit() {
		return (getCurrentPageNumber() * pageHeight) - (vOffset * multiplierY)
				+ LIMITERHEIGHT/2;
	}

	public int getPageNumber(float pointy){
		return ((int)pointy)/ ((int)pageHeight);
	}

	public Boolean isHScrolling() {
		return scrollingX;
	}

	public void setHScrolling(Boolean scrollingX) {
		this.scrollingX = scrollingX;
	}

	public boolean canDisplaceVertically(){
		float vertOffset = getScrollVertOffset();
		float lowerLimit = getVLowerLimit();
		float upperLimit = getVUpperLimit();
		return (vertOffset >= upperLimit
				&& vertOffset <= lowerLimit);
	}

    public boolean canDisplaceHorizontally(){
        float hOffset = getScrollHOffset();
        float lowerLimit = getHLowerLimit();
        float upperLimit = getHUpperLimit();
        return (hOffset >= upperLimit
                && hOffset <= lowerLimit);
    }

    public void changeVertOffset( float dy ){
        float vertOffset = getScrollVertOffset();
        float lowerLimit = getVLowerLimit();
        float upperLimit = getVUpperLimit();

        vertOffset += dy; //displacement
        vertOffset = (vertOffset < upperLimit) ? upperLimit : vertOffset;
        vertOffset = (vertOffset > lowerLimit) ? lowerLimit : vertOffset;

        setVertOffset(vertOffset);
    }

    public void changeHOffset( float diff ){
        float hOffset = getScrollHOffset();
        float lowerLimit = getHLowerLimit();
        float upperLimit = getHUpperLimit();

        hOffset += diff; //displacement
        hOffset = (hOffset < upperLimit) ? upperLimit : hOffset;
        hOffset = (hOffset > lowerLimit) ? lowerLimit : hOffset;

        setHOffset(hOffset);
    }
}
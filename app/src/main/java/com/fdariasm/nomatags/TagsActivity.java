package com.fdariasm.nomatags;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.fdariasm.nomatags.views.AddTagsFragment;

/**
 * Created by fdariasm on 04/11/2015.
 */
public class TagsActivity extends AppCompatActivity {

    private AddTagsFragment tagsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.manage_tags);
        FragmentManager sfm = getSupportFragmentManager();

        tagsFragment = (AddTagsFragment) sfm.findFragmentById(R.id.tags_fragment);

        if (null == tagsFragment) {
            tagsFragment = new AddTagsFragment();
            sfm.beginTransaction().add(R.id.tags_fragment, tagsFragment).commit();
        }
    }
}

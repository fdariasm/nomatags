package com.fdariasm.nomatags.listeners;

import com.fdariasm.nomatags.events.DocChangedEvent;

import java.io.Serializable;

public interface DocumentListener extends Serializable{
	
	void documentChanged(DocChangedEvent event);
}

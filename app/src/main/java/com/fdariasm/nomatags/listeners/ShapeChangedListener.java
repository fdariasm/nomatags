package com.fdariasm.nomatags.listeners;

import com.fdariasm.nomatags.events.ShapeChangedEvent;

public interface ShapeChangedListener {
	void shapeChanged(ShapeChangedEvent event);
}

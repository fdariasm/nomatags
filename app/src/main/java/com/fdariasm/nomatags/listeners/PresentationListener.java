package com.fdariasm.nomatags.listeners;

import com.fdariasm.nomatags.events.PresentationEvent;

public interface PresentationListener {
	
	void presentationChanged(PresentationEvent event);

}

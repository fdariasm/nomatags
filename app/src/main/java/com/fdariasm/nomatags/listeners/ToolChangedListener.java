package com.fdariasm.nomatags.listeners;

import com.fdariasm.nomatags.events.ToolChangedEvent;

public interface ToolChangedListener{

	void toolChanged(ToolChangedEvent event);
}

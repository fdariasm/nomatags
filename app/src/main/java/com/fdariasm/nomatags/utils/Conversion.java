package com.fdariasm.nomatags.utils;

import android.database.Cursor;
import android.view.MotionEvent;

import com.fdariasm.nomatags.events.DrawEvent;
import com.fdariasm.nomatags.models.components.Point;

import java.util.Date;
import java.util.LinkedList;

public class Conversion {
	
	private Conversion(){}
	
	/**
	 * Transforms an android event to a motion event, getting just the last point
	 * @param event
	 * @return
	 */
	public static DrawEvent getEvent( MotionEvent event ){
		DrawEvent nEvent = new DrawEvent();
		
		nEvent.setType(getType(event.getAction()));
		nEvent.setX(event.getX());
		nEvent.setY(event.getY());
		
		return nEvent;
	}

	/**
	 * Transforms an android event to a motion event, getting half the
	 * historical points of the android event
	 * @param event
	 * @return
	 */
	public static DrawEvent getEventExtended(MotionEvent event){
		DrawEvent nEvent = new DrawEvent();
		
		nEvent.setType(getType(event.getAction()));
		nEvent.setX(event.getX());
		nEvent.setY(event.getY());
		
		LinkedList<Point> points = new LinkedList<Point>();
		for(int i = 0; i < event.getHistorySize(); i+=2){
			float x = event.getHistoricalX(i);
			float y = event.getHistoricalY(i);
			Point current = new Point(x, y);
			points.add(current);
		}
		nEvent.setPoints(points);
		
		return nEvent;
	}

	private static DrawEvent.Type getType(int action) {
		switch(action){
		case MotionEvent.ACTION_DOWN:
			return DrawEvent.Type.DOWN;
		case MotionEvent.ACTION_MOVE:
			return DrawEvent.Type.MOVE;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			return DrawEvent.Type.UP;
		case MotionEvent.ACTION_CANCEL:
			return DrawEvent.Type.CANCEL;
		}
		return DrawEvent.Type.OTHER;
	}

	public static Long persistDate(Date date) {
		if (date != null) {
			return date.getTime();
		}
		return null;
	}

	public static Date loadDate(Cursor cursor, int index) {
		if (cursor.isNull(index)) {
			return null;
		}
		return new Date(cursor.getLong(index));
	}
}

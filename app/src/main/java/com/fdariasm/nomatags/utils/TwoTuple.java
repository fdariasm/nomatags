package com.fdariasm.nomatags.utils;

public class TwoTuple <T> {
	
	public final T first;
	
	public final T second;
	
	public TwoTuple(T first, T sec) {
		this.first = first;
		this.second = sec;
	}

}

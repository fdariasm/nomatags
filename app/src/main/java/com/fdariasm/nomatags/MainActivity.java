package com.fdariasm.nomatags;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.fdariasm.nomatags.views.ListDocumentsFragment;
import com.fdariasm.nomatags.views.ListTagsFragment;

public class MainActivity extends AppCompatActivity implements ListTagsFragment.OnTagSelected{
    private ListDocumentsFragment documentFragment;

    private ListTagsFragment tagsFragment;

    private static final String TAG = MainActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_init);

        FragmentManager sfm = getSupportFragmentManager();

        documentFragment = (ListDocumentsFragment) sfm.findFragmentById(R.id.content_frame);

        if(null == documentFragment){
            documentFragment = new ListDocumentsFragment();
            sfm.beginTransaction().add(R.id.content_frame, documentFragment).commit();
        }

        tagsFragment = (ListTagsFragment) sfm.findFragmentById(R.id.tags_frame);

        if(null == tagsFragment){
            tagsFragment = new ListTagsFragment();
            sfm.beginTransaction().add(R.id.tags_frame, tagsFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_options, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(searchItem);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                android.util.Log.i(TAG, "onQueryTextSubmit: " + query);

                if (null != query && !query.isEmpty())
                    documentFragment.filterByName(query);
                else
                    documentFragment.resetFilter();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (null == newText || newText.isEmpty())
                    documentFragment.resetFilter();
                return true;
            }
        });

        MenuItem searchItemTag = menu.findItem(R.id.action_search_tag);
        SearchView searchTagView =
                (SearchView) MenuItemCompat.getActionView(searchItemTag);


        searchTagView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                android.util.Log.i(TAG, "onQueryTextSubmit: " + query);
                tagsFragment.filterByName(query);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (null == newText || newText.isEmpty())
                    tagsFragment.filterByName("");
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.new_note:
                startActivity(new Intent(this, EditActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void tagSelected(int idTag) {
        android.util.Log.i(TAG, "idTag selected: " + idTag);
        documentFragment.filterByTag(idTag);
    }

    @Override
    public void tagDeleted() {
        documentFragment.resetFilter();
    }
}

package com.fdariasm.nomatags;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.fdariasm.nomatags.controllers.CanvasController;
import com.fdariasm.nomatags.events.DocChangedEvent;
import com.fdariasm.nomatags.events.PresentationEvent;
import com.fdariasm.nomatags.events.ToolChangedEvent;
import com.fdariasm.nomatags.listeners.DocumentListener;
import com.fdariasm.nomatags.listeners.PresentationListener;
import com.fdariasm.nomatags.models.AppState;
import com.fdariasm.nomatags.models.DocumentShapes;
import com.fdariasm.nomatags.models.ToolState;
import com.fdariasm.nomatags.models.actions.UndoManager;
import com.fdariasm.nomatags.models.components.Tag;
import com.fdariasm.nomatags.models.process.ImportImage;
import com.fdariasm.nomatags.models.process.SaveDocument;
import com.fdariasm.nomatags.views.AddTagsFragment;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import yuku.ambilwarna.AmbilWarnaDialog;


public class EditActivity extends AppCompatActivity {

    public static final String KEY_DOCUMENT = "Document_Shapes";
    private static final int RESULT_TAGS = 1;
    private static final int RESULT_LOAD_IMAGE = 2;
    private CanvasController mController;

	private UndoManager undoManager = UndoManager.getManager();

    private boolean docModified = false;
    private ActionBar actionBar;

    public EditActivity(){

    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		actionBar = getSupportActionBar();
		mController = (CanvasController) getSupportFragmentManager()
				.findFragmentById(R.id.mainframe); 

		if (null == mController) {
			mController = new CanvasController();

            Bundle extras = getIntent().getExtras();
            if(null != extras && null != extras.getSerializable(KEY_DOCUMENT)){
                final Integer id = (Integer) extras.getSerializable(KEY_DOCUMENT);
                SaveDocument saveDocument = SaveDocument.getInstance(getApplicationContext());
                saveDocument.loadDocument(id, new SaveDocument.OnDocumentLoadedListener() {
                    @Override
                    public void updateState(DocumentShapes document) {
                        if (null == document) {
                            Toast.makeText(EditActivity.this, "No se pudo cargar documento id: " + id, Toast.LENGTH_LONG);
                            return;
                        }

                        mController.initDocument(document);
                        mController.getState().addDocumentListener(changeListener);
                        mController.getState().addPresentationListener(presentationListener);
                        findViewById(R.id.progressBar).setVisibility(View.GONE);
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.mainframe, mController).commit();
                    }
                });
            }else {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                getSupportFragmentManager().beginTransaction()
						.add(R.id.mainframe, mController).commit();
                mController.getState().addDocumentListener(changeListener);
                mController.getState().addPresentationListener(presentationListener);
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.editor_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_stroke);
        Spinner spinner =
                (Spinner) MenuItemCompat.getActionView(searchItem);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(strokeListener);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		AppState state = mController.getState();
		switch(item.getItemId()){
		case R.id.action_undo:
			if (!undoManager.isUndoEmpty()) {
				undoManager.popUndo().unexecute();
			}
			return true;
		case R.id.action_redo:
			if (!undoManager.isRedoEmpty()) {
				undoManager.popRedo().execute();
			}
			return true;
		case R.id.option_line:
			mController.toolChangedEvent(ToolChangedEvent.DrawingTool.LINE);
			return true;
		case R.id.option_ellipse:
				mController.toolChangedEvent(ToolChangedEvent.DrawingTool.ELLIPSE);
				return true;
		case R.id.option_square:
			mController.toolChangedEvent(ToolChangedEvent.DrawingTool.SQUARE);
			return true;
		case R.id.option_curve:
			mController.toolChangedEvent(ToolChangedEvent.DrawingTool.CURVE);
			return true;
		case R.id.option_select:
			mController.toolChangedEvent(ToolChangedEvent.DrawingTool.SELECT);
			return true;
		case R.id.option_memory:
			Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
			Debug.getMemoryInfo(memoryInfo);

			String memMessage = String.format("App Memory: Pss=%.2f MB\nPrivate=%.2f MB\nShared=%.2f MB",
					memoryInfo.getTotalPss() / 1024.0,
					memoryInfo.getTotalPrivateDirty() / 1024.0,
					memoryInfo.getTotalSharedDirty() / 1024.0);
			android.util.Log.i("log_tag", memMessage);
			android.util.Log.i("log_tag", "Total memory: " +Runtime.getRuntime().totalMemory());
			android.util.Log.i("log_tag", "Allocated memory: " + 
					(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
			android.util.Log.i("log_tag", "max memory: " + Runtime.getRuntime().maxMemory());
			android.util.Log.i("log_tag", "Native memory: " + Debug.getNativeHeapAllocatedSize());
			return true;
		case R.id.action_addpage:
			state.addNewPage();
			return true;
		case R.id.option_delete:
			mController.deleteSelectionEvent();
			return true;
		case R.id.option_importimage:
			loadImage();
			return true;
		case R.id.option_copy:
			mController.copySelectionEvent();
			return true;
		case R.id.option_paste:
			mController.pasteEvent();
			return true;
		case R.id.option_addtag:
			inputTag();
			return true;
		case R.id.option_save:
			saveDocument();
			return true;
		case R.id.change_color:
			showChangeColor();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

    private void showChangeColor() {
        final ToolState toolState =
                mController.getState().getToolState();
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, toolState.getPaintColor(), new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                toolState.setPaintColor(color);
            }

            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
            }
        });
        dialog.show();
    }

	private void saveDocument() {
		final AppState state = mController.getState();

		LayoutInflater li = LayoutInflater.from(this);

		View promptsView = li.inflate(R.layout.save_doc, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.save_input);

		String name = state.getDocument().getName();
		if(null != name)
			userInput.setText(name);

		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
                                String docName = userInput.getText().toString();
                                if(null != docName && !docName.isEmpty()) {
                                    DocumentShapes document = state.getDocument();
                                    document.setName(docName);
                                    document.setPages(state.getPresentation().getPages());
                                    SaveDocument saveDocument = SaveDocument.getInstance(getApplicationContext());

                                    saveDocument.saveDocumentState(document);
                                    docModified = false;
                                }
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

    @Override
    public void onBackPressed() {
        AppState state = mController.getState();
        DocumentShapes document = state.getDocument();

        if(docModified && null == document.getId()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.exit_message))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.exit_yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            EditActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.exit_no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        AppState state = mController.getState();
        DocumentShapes document = state.getDocument();

        if(null != document.getId() && docModified) {
            SaveDocument saveDocument = SaveDocument.getInstance(getApplicationContext());
            document.setPages(state.getPresentation().getPages());
            saveDocument.saveDocumentState(document);
            docModified = false;
        }

        super.onPause();
    }

    @Override
	protected void onStop() {
		super.onStop();
	}

	private void inputTag() {
        DocumentShapes document = mController.getState().getDocument();

        Intent intent = new Intent();
        Bundle b = new Bundle();
        b.putSerializable(AddTagsFragment.TAGS_KEY, new HashSet<Tag>(document.getTags()));
        intent.putExtras(b);
        intent.setClass(this, TagsActivity.class);
        startActivityForResult(intent, RESULT_TAGS);
	}

    private DocumentListener changeListener = new DocumentListener(){

        @Override
        public void documentChanged(DocChangedEvent event) {
            docModified = true;
        }
    };

    private PresentationListener presentationListener = new PresentationListener() {
        @Override
        public void presentationChanged(PresentationEvent event) {
            docModified = true;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_TAGS && resultCode == RESULT_OK && null != data) {
            Bundle extras = data.getExtras();
            Set<Tag> tags = (HashSet<Tag>) extras.getSerializable(AddTagsFragment.TAGS_KEY);

            android.util.Log.i("activityResult", Arrays.toString(tags.toArray()));

            mController.getState().getDocument().setTags(tags);
            docModified = true;
        }else if (requestCode == RESULT_LOAD_IMAGE && resultCode == FragmentActivity.RESULT_OK && null != data) {
            ImportImage importImage = new ImportImage(mController.getState());
            importImage.startImport(data, getContentResolver());
        }
    }

    public void loadImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                getString(R.string.select_picture)), RESULT_LOAD_IMAGE);
    }

    private AdapterView.OnItemSelectedListener strokeListener = new AdapterView.OnItemSelectedListener(){
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int pos, long id) {
            android.util.Log.i("OnItemSelectedListener", pos + "");
            ToolState toolState = mController.getState().getToolState();

            toolState.setPaintStrokeWidth((pos + 1) * 3);

        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Another interface callback
        }
    };
}
